#include "cxx-utils.hh"

bool assertion_error::use_colour = false;

defer_type_operator_lhs defer_type_operator_lhs::instance;
tempset_operator_lhs    tempset_operator_lhs::instance;
bool                    $$debug_break = false;

void todo_impl(const char* file, int line, const char* pretty_function) {
    throw std::runtime_error{fmt::format("Internal Error: TODO\n"
                                         "    In internal file {}:{}\n"
                                         "    In function {}\n",
        file, line, pretty_function)};
}
