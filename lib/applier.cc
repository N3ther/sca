#include "applier.hh"

#include <iostream>
#include <random>

namespace sca {
void applier::init_from_parser(parser& p) {
#ifdef ENABLE_NATIVE_COMPILATION
    auto fsms = native ? sca::compiler::compile_to_native(p, dump_ir) : sca::compiler::compile(p);
#else
    auto fsms = sca::compiler::compile(p);
#endif
    rules.resize(fsms.size());
    for (u64 i = 0; i < fsms.size(); ++i) {
#ifdef ENABLE_NATIVE_COMPILATION
        if (native) ASSERT(fsms[i].native_handle);
#endif
        rules[i] = {std::move(p.rules[i]), std::move(fsms[i])};
    }
}

word applier::apply(word w) const {
    for (const auto& rule : rules) apply(rule, w);
    return w;
}

void applier::apply(const rule& r, std::vector<word>& words) const {
    PARALLEL for (u64 i = 0; i < words.size(); ++i) apply(r, words[i]);
}

auto applier::apply(std::vector<word> words) const -> std::vector<word> {
    for (auto& r : rules) apply(r, words);
    return words;
}

void applier::apply(const rule& r, word& w) const {
    auto& [rule, machine] = r;
#ifdef ENABLE_NATIVE_COMPILATION
    auto res = native ? machine.template match<true>(w) : machine.template match<false>(w);
#else
    auto res  = machine.template match<false>(w);
#endif
    if (res.positions.empty()) return;
    switch (rule.type) {
        case parser::rule_type::substitution:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it) {
                const parser::element& el = rule.output.els[it->which];
                w.data.replace(it->start, it->len, el.percentage == 0 ? el.text.data : el.construct_text());
            }
            break;
        case parser::rule_type::epenthesis:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it) {
                const parser::element& el = rule.output.els[it->which];
                w.data.insert(it->start, el.percentage == 0 ? el.text.data : el.construct_text());
            }
            break;
        case parser::rule_type::deletion:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it)
                w.data.erase(it->start, it->len);
            break;
        case parser::rule_type::metathesis:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it)
                w.data.replace(it->start, it->len, rule.input.els[it->which].text.reverse());
            break;
        case parser::rule_type::reduplication:
            for (auto it = res.positions.rbegin(); it != res.positions.rend(); ++it) {
                std::u32string_view s{w.data.data() + it->start, it->len};
                for (u64 i = 0; i < rule.reps; i++) w.data.insert(it->start, s);
            }
            break;
    }
}

} // namespace sca
