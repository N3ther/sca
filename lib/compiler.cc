#include "compiler.hh"

#include "unicode.hh"

#include <set>
#include <sstream>

#ifdef __linux__
#    include <sys/mman.h>
#endif

namespace sca {
/// Static
using el_t = parser::element_type;

///
/// Miscellaneous
///

void compiler::dump() {
    machine.dump();
}

void fsm::dump() const {
    fmt::print("===========\n"
               "=== FSM ===\n"
               "===========\n");
    std::set<u32> addresses;
    for (u64 i = 0; i < bytecode.size(); i++) {
        fmt::print("[{:08X}]: ", i);
        switch (auto opcode = bytecode[i]; opcode) {
            case fsm::i_one: {
                i++;
                if (i > bytecode.size() - 4) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                char32_t c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                for (u64 j = i - 1; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("              one     {} (U+{:04X})\n", (unicode::is_control(c) ? "?" : to_utf8(c).c_str()), u32(c));
                i += 3;
            } break;
            case fsm::i_any:
                fmt::print("{:02X}                           any\n", fsm::i_any);
                break;
            case fsm::i_try: {
                i++;
                if (i > bytecode.size() - 8) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                char32_t c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                i += 4;
                char32_t c2;
                std::memcpy(&c2, bytecode.data() + i, sizeof c2);
                addresses.insert(c2);
                for (u64 j = i - 5; j < i; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("              try     [{:08X}]\n", u32(c));

                fmt::print("[{:08X}]: ", i - 1);
                for (u64 j = i; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("                 addr    [{:08X}]\n", u32(c2));
                i += 3;
            } break;
            case fsm::i_opt: {
                i++;
                if (i > bytecode.size() - 4) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                char32_t c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                for (u64 j = i - 1; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("              opt     [{:08X}]\n", u32(c));
                i += 3;
            } break;
            case fsm::i_neg: {
                i++;
                if (i > bytecode.size() - 4) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                char32_t c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                for (u64 j = i - 1; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                fmt::print("              neg     [{:08X}]\n", u32(c));
                i += 3;
            } break;
            case fsm::i_capture_start:
                i++;
                if (i > bytecode.size() - 4) {
                    fmt::print("\n !!! Decode error: missing bytes !!!\n");
                    return;
                }
                u32 c;
                std::memcpy(&c, bytecode.data() + i, sizeof c);
                fmt::print("{:02X} {:02X} {:02X} {:02X} {:02X}               start   {}\n",
                    fsm::i_capture_start, bytecode[i], bytecode[i + 1], bytecode[i + 2], bytecode[i + 3],
                    c);
                i += 3;
                break;
            case fsm::i_capture_end:
                fmt::print("{:02X}                           end\n", fsm::i_capture_end);
                break;
            case fsm::i_accept:
                fmt::print("{:02X}                           acc\n", fsm::i_accept);
                break;
            case fsm::i_reject:
                fmt::print("{:02X}                           rej\n", fsm::i_reject);
                break;
            case fsm::i_boundary:
                fmt::print("{:02X}                           bnd\n", fsm::i_boundary);
                break;
            case fsm::i_match_class:
            case fsm::i_match_class_2:
            case fsm::i_match_class_3:
            case fsm::i_match_class_4:
            case fsm::i_match_capture:
            case fsm::i_match_capture_2:
            case fsm::i_match_capture_3:
            case fsm::i_match_capture_4: {
                static constexpr const char* opcode_suffix = " 234";

                i++;
                u32 sz;
                std::memcpy(&sz, bytecode.data() + i, sizeof sz);
                fmt::print("{:02X} {:02X} {:02X} {:02X} {:02X}               {}{}   {}\n",
                    opcode, bytecode[i], bytecode[i + 1], bytecode[i + 2], bytecode[i + 3],
                    opcode < fsm::i_match_capture ? "mcls" : "mcap", opcode_suffix[(opcode - fsm::i_match_class) % 4], sz);

                i += 4;
                for (u8 j = 0; j < sz * (((opcode - fsm::i_match_class) % 4) + 1); j++, i += 4) {
                    u32 c;
                    std::memcpy(&c, bytecode.data() + i, sizeof c);
                    fmt::print("[{:08X}]: ", (unsigned) i);
                    for (u64 k = i; k < i + 4; k++) fmt::print("{:02X} ", bytecode[k]);
                    fmt::print("                 char    {} (U+{:04X})\n", unicode::is_control(c) ? "?" : to_utf8(c).c_str(), u32(c));
                }
                i--;
            } break;
            default:
                if (i > bytecode.size() - 4) fmt::print("Invalid instruction: {:02X}\n", u32(bytecode[i]));
                else {
                    u32 c;
                    std::memcpy(&c, bytecode.data() + i, sizeof c);
                    if (!addresses.contains(char32_t(i))) fmt::print("Invalid instruction: {:02X}\n", u32(bytecode[i]));
                    else {
                        addresses.insert(c);
                        for (u64 j = i; j < i + 4; j++) fmt::print("{:02X} ", bytecode[j]);
                        fmt::print("                 addr    [{:08X}]\n", u32(c));
                        i += 3;
                    }
                }
        }
    }
}

match_context::match_context(const fsm& _machine, const word& _w)
    : machine(_machine), w(_w) {
    curr = w.data.data();
    end  = w.data.data() + w.data.size();
    ip   = machine.bytecode.data();
}

void match_context::reset(const char32_t* ptr) {
    ip   = machine.bytecode.data();
    curr = ptr;
}

fsm::fsm(fsm&& other) noexcept {
    bytecode            = std::move(other.bytecode);
#ifdef ENABLE_NATIVE_COMPILATION
    native_handle       = other.native_handle;
    other.native_handle = nullptr;
#endif
}
fsm& fsm::operator=(fsm&& other) noexcept {
    if (this == std::addressof(other)) return *this;
    bytecode            = std::move(other.bytecode);
#ifdef ENABLE_NATIVE_COMPILATION
    native_handle       = other.native_handle;
    other.native_handle = nullptr;
#endif
    return *this;
}

fsm::~fsm() {
#ifdef ENABLE_NATIVE_COMPILATION
    if (native_handle) munmap(reinterpret_cast<void*>(native_handle), 4096);
#endif
}

#ifdef ENABLE_NATIVE_COMPILATION
auto fsm::execute(const word& w) const -> match_result {
    match_data m;
    m.curr = w.data.data();
    m.end  = w.data.data() + w.data.size();
    match_result res;
    for (;;) {
        if (m.end == m.curr) return res;
        if (native_handle(&m)) {
            u64               start_pos = u64(m.cap_start - w.data.data());
            match_result::pos p{
                .start = start_pos,
                .len   = u64(m.cap_end - w.data.data()) - start_pos,
                .which = m.capture_index,
            };
            res.positions.push_back(p);
        }
        m.curr++;
    }
}
#endif

auto fsm::interpret(const word& w) const -> match_result {
    match_context m{*this, w};
    match_result  res;
    for (;;) {
        if (m.end == m.curr) return res;
        auto* curr = m.curr;
        if (m.do_match()) {
            u64               start_pos = u64(m.cap_start - m.w.data.data());
            match_result::pos p{
                .start = start_pos,
                .len   = u64(m.cap_end - m.w.data.data()) - start_pos,
                .which = m.capture_index,
            };
            res.positions.push_back(p);
        }
        m.reset(curr + 1);
    }
}

void match_context::discard() {
    if (curr != end) curr++;
}

///
/// Compiler Implementation
///

u64 compiler::alloc_addr() {
    auto& bc   = machine.bytecode;
    u64   addr = bc.size();
    bc.resize(bc.size() + 4);
    return addr;
}

template <typename V>
void compiler::store_addr(u64 addr, V value) {
    static_assert(sizeof(V) == sizeof(u32));
    std::memcpy(machine.bytecode.data() + addr, &value, sizeof value);
}

template <fsm::opcode o, typename... args_t, bool... conds_t>
void compiler::emit(args_t&&... args) {
    auto& bc = machine.bytecode;

    if constexpr (o == fsm::i_any) bc.push_back(fsm::i_any);
    else if constexpr (o == fsm::i_capture_start) bc.push_back(fsm::i_capture_start);
    else if constexpr (o == fsm::i_capture_end) bc.push_back(fsm::i_capture_end);
    else if constexpr (o == fsm::i_accept) bc.push_back(fsm::i_accept);
    else if constexpr (o == fsm::i_reject) bc.push_back(fsm::i_reject);
    else if constexpr (o == fsm::i_boundary) bc.push_back(fsm::i_boundary);

    else if constexpr (o == fsm::i_neg || o == fsm::i_opt) {
        static_assert(sizeof...(args) == 1 && (std::is_same_v<std::remove_cvref_t<args_t>, parser::element> && ...));
        bc.push_back(o);
        u64 addr = alloc_addr();
        compile_el(args...);
        bc.push_back(fsm::i_accept);
        store_addr(addr, u32(bc.size()));
    }

    else if constexpr (o == fsm::i_one) {
        static_assert(sizeof...(args) == 1 && (std::is_same_v<std::remove_cvref_t<args_t>, char32_t> && ...));
        bc.push_back(fsm::i_one);
        store_addr(alloc_addr(), args...);
    }

    else if constexpr (o == fsm::i_try) emit_try(std::forward<args_t>(args)...);

    else if constexpr (o == fsm::i_match_class) emit_i_match_class<1>(args...);
    else if constexpr (o == fsm::i_match_class_2) emit_i_match_class<2>(args...);
    else if constexpr (o == fsm::i_match_class_3) emit_i_match_class<3>(args...);
    else if constexpr (o == fsm::i_match_class_4) emit_i_match_class<4>(args...);
    else if constexpr (o == fsm::i_match_capture) emit_i_match_capture<1>(args...);
    else if constexpr (o == fsm::i_match_capture_2) emit_i_match_capture<2>(args...);
    else if constexpr (o == fsm::i_match_capture_3) emit_i_match_capture<3>(args...);
    else if constexpr (o == fsm::i_match_capture_4) emit_i_match_capture<4>(args...);

    else CONSTEXPR_NOT_IMPLEMENTED("");
}

void compiler::emit_try(const parser::element::elements& els, bool capture) {
    auto& bc = machine.bytecode;
    bc.push_back(fsm::i_try);
    u64 end_addr = alloc_addr();

    u64 last_addr{};
    for (u64 i = 0; i < els.size(); i++) {
        last_addr = alloc_addr();

        /// Emit the capture index.
        if (capture) {
            emit<fsm::i_capture_start>();
            store_addr(alloc_addr(), u32(i));
        }

        compile_el(els[i]);
        emit<fsm::i_accept>();
        store_addr(last_addr, u32(bc.size()));
    }

    store_addr(last_addr, u32(FSM_TRY_REJECT));
    store_addr(end_addr, u32(bc.size()));

    if (capture) emit<fsm::i_capture_end>();
}

/// Check whether all elements in `el` are text elements containing `num` codepoints.
static bool is_class_of_size(const parser::element& el, u64 num) {
    return std::all_of(el.els.begin(), el.els.end(), [num](const parser::element& el) {
        return el.type == parser::element_type::text && el.text.data.size() == num;
    });
}

template <u64 elems_length>
void compiler::emit_i_match_class(const parser::element& el) {
    static_assert(elems_length >= 1 && elems_length <= 4);
    static constexpr char32_t instrs[4] = {fsm::i_match_class, fsm::i_match_class_2, fsm::i_match_class_3, fsm::i_match_class_4};

    auto& bc = machine.bytecode;

    /// Make sure we have elements to emit.
    if (el.els.empty()) return;

    /// Maybe we can optimise this class by removing a common prefix or suffix.
    if constexpr (elems_length > 1) {
        parser::element new_el;
        char32_t        c;

        /// Check to see if we can remove the first character.
        c = el.els[0].text.data[0];
        if (std::all_of(el.els.begin(), el.els.end(), [c](const parser::element& el) { return el.text.data[0] == c; })) {
            /// Remove the first character from all elements.
            for (const auto& item : el.els) {
                word w;
                w += std::u32string_view{item.text.data.data() + 1, item.text.size() - 1};
                auto w_el = parser::element::from_text(std::move(w));
                if (!contains(new_el.els, w_el)) new_el.els.push_back(std::move(w_el));
            }

            /// Emit the character removed.
            emit<fsm::i_one>(c);

            /// Emit the remaining elements.
            emit_i_match_class<elems_length - 1>(new_el);
            return;
        }

        /// Check to see if we can remove the last character.
        c = el.els[0].text.data.back();
        if (std::all_of(el.els.begin(), el.els.end(), [c](const parser::element& el) { return el.text.data.back() == c; })) {
            /// Remove the last character from all elements.
            for (const auto& item : el.els) {
                word w;
                w += std::u32string_view{item.text.data.data(), item.text.size() - 1};
                auto w_el = parser::element::from_text(std::move(w));
                if (!contains(new_el.els, w_el)) new_el.els.push_back(std::move(w_el));
            }

            /// Emit the remaining elements.
            emit_i_match_class<elems_length - 1>(new_el);

            /// Emit the character removed.
            emit<fsm::i_one>(c);
            return;
        }
    }

    /// Append the match_instruction.
    bc.push_back(instrs[elems_length - 1]);

    /// Allocate enough memory for the size of the class as well as all elements.
    u64 offs = bc.size(); /// Current offset
    bc.resize(offs + sizeof(u32) /** class size **/ + sizeof(u32) * elems_length * el.els.size() /** elems size in bytes **/);

    /// Store the size of the class.
    store_addr(offs, u32(el.els.size()));
    offs += 4;

    /// Store the elements
    for (const auto& item : el.els) {
        for (u64 i = 0; i < elems_length; i++) {
            store_addr(offs, u32(item.text.data[i]));
            offs += 4;
        }
    }
}

template <u64 elems_length>
void compiler::emit_i_match_capture(const parser::element& el) {
    static_assert(elems_length >= 1 && elems_length <= 4);
    static constexpr char32_t instrs[4] = {fsm::i_match_capture, fsm::i_match_capture_2, fsm::i_match_capture_3, fsm::i_match_capture_4};

    auto& bc = machine.bytecode;

    /// Append the match_instruction.
    bc.push_back(instrs[elems_length - 1]);

    /// Allocate enough memory for the size of the class as well as all elements.
    u64 offs = bc.size(); /// Current offset
    bc.resize(offs + sizeof(u32) /** class size **/ + sizeof(u32) * elems_length * el.els.size() /** elems size in bytes **/);

    /// Store the size of the class.
    store_addr(offs, u32(el.els.size()));
    offs += 4;

    /// Store the elements
    for (const auto& item : el.els) {
        for (u64 i = 0; i < elems_length; i++) {
            store_addr(offs, u32(item.text.data[i]));
            offs += 4;
        }
    }
}

void compiler::compile_el(const parser::element& el) {
    switch (el.type) {
        case el_t::empty: return;
        case el_t::text:
            for (const auto& item : el.text.data) {
                emit<fsm::i_one>(item);
            }
            return;
        case el_t::negative: {
            if (el.els.empty()) throw compiler::error("Cannot compile empty negative");
            emit<fsm::i_neg>(el.els[0]);
            return;
        }
        case el_t::chain:
            if (el.els.empty()) throw compiler::error("Cannot compile empty chain");
            for (const auto& item : el.els) compile_el(item);
            return;
        case el_t::cclass: {
            if (el.els.empty()) throw compiler::error("Cannot compile empty class");
            if (el.els.size() > UINT32_MAX) throw compiler::error(fmt::format("Too many elements in class. Maximum is {}.", UINT32_MAX));

            /// Optimise groups whose elements all have the same length.
            /// Otherwise, use a try.
            if (is_class_of_size(el, 1)) emit<fsm::i_match_class>(el);
            else if (is_class_of_size(el, 2)) emit<fsm::i_match_class_2>(el);
            else if (is_class_of_size(el, 3)) emit<fsm::i_match_class_3>(el);
            else if (is_class_of_size(el, 4)) emit<fsm::i_match_class_4>(el);
            else emit<fsm::i_try>(el.els);
            return;
        }
        case el_t::optional: {
            if (el.els.empty()) throw compiler::error("Cannot compile empty optional");
            emit<fsm::i_opt>(el.els[0]);
            return;
        }
        case el_t::wildcard:
            emit<fsm::i_any>();
            return;
        case el_t::list: throw compiler::error("To compile a list, use compiler::compile_capture_group instead");
        case el_t::word_boundary:
            emit<fsm::i_boundary>();
            return;
        case el_t::syllable_boundary:
            throw compiler::error("Sorry, unimplemented. Cannot compile syllable boundaries.");
            return;
    }
    throw std::runtime_error{fmt::format("{} called with illegal element type '{}'", __PRETTY_FUNCTION__, enum_name(el.type))};
}

void compiler::compile_capture_group(const parser::element& el) {
    if (el.type != parser::element_type::list
        && el.type != parser::element_type::cclass)
        throw compiler::error(fmt::format("Capture group must be a list or class, but was {}", enum_name(el.type)));
    if (el.els.empty()) throw compiler::error("Cannot compile empty group");

    /// If we only have one input element, then we don't need a try.
    /// Optimise groups whose elements all have the same length.
    /// Otherwise, use a try.
    if (el.els.size() == 1) {
        emit<fsm::i_capture_start>();
        store_addr(alloc_addr(), u32(0));
        compile_el(el.els[0]);
        emit<fsm::i_capture_end>();
    } else if (is_class_of_size(el, 1)) emit<fsm::i_match_capture>(el);
    else if (is_class_of_size(el, 2)) emit<fsm::i_match_capture_2>(el);
    else if (is_class_of_size(el, 3)) emit<fsm::i_match_capture_3>(el);
    else if (is_class_of_size(el, 4)) emit<fsm::i_match_capture_4>(el);
    else emit<fsm::i_try>(el.els, true);
}

auto compiler::compile(const parser& p) -> std::vector<fsm> {
    std::vector<fsm> fsms;
    fsms.reserve(p.rules.size());
    for (const auto& item : p.rules) {
        compiler c;
        c.compile_el(item.ctx.pre);
        if (item.type == parser::rule_type::epenthesis) {
            c.emit<fsm::i_capture_start>();
            c.store_addr(c.alloc_addr(), u32(0));
        } else c.compile_capture_group(item.input);
        c.compile_el(item.ctx.post);
        c.emit<fsm::i_accept>();
        fsms.push_back(std::move(c.machine));
    }
    return fsms;
}

#define ip32(offs) ({char32_t _c; std::memcpy(&_c, ip + offs * sizeof _c, sizeof _c); _c; })
#define READ_ADDR() \
    ({auto _char = ip32(0); \
    ip += sizeof(char32_t); _char; })

#ifdef ENABLE_NATIVE_COMPILATION
/// rdi:  match_data*
/// rsi:  curr
/// rdx:  end
/// r10d: cap_idx
/// r9:   cap_start
/// r11:  cap_end
struct code_generator {
    u8* const mem;
    u8*       $;
    u8*       rej_addr;
    u8*       acc_addr;

    enum jmp_type {
        jmp_eq  = 0x4,
        jmp_ne  = 0x5,
        jmp_lt  = 0xc,
        jmp_ge  = 0xd,
        jmp_jmp = 999999,
    };

    code_generator(uint8_t* m) : mem(m) { $ = mem; }

    /// Insert bytes.
    template <u8... args>
    void i() { ((*$++ = args), ...); }

    /// Insert character or address at the given address.
    template <typename T>
    void i4(T c, u8* addr) {
        static_assert(sizeof(T) == 4);
        addr[0] = c & 0xff;
        addr[1] = (c >> 8) & 0xff;
        addr[2] = (c >> 16) & 0xff;
        addr[3] = (c >> 24) & 0xff;
    }

    /// Insert character or address.
    template <typename T>
    void i4(T c) {
        static_assert(sizeof(T) == 4);
        *$++ = c & 0xff;
        *$++ = (c >> 8) & 0xff;
        *$++ = (c >> 16) & 0xff;
        *$++ = (c >> 24) & 0xff;
    }

    /// Fixup an unconditional near jmp instruction to jump to the current address.
    void fixup_near_jmp(u8* addr) { i4(u32($ - addr) - 5, addr); }

    /// Emit a jmp instruction to the specified address.
    template <jmp_type j, bool force_near_jmp = false>
    void jmp(const u8* address) {
        if (address == $) return;
        i64 _diff = address - $;
        ASSERT(_diff >= -static_cast<i64>(UINT32_MAX) - 1 && _diff <= static_cast<i64>(UINT32_MAX),
            "jmp: address out of range. Too far away: {}", _diff);

        i32 diff = _diff;
        if constexpr (j == jmp_jmp) {
            if (!force_near_jmp && diff >= -126 && diff <= 129) {
                *$++ = 0xeb;
                *$++ = (i8) diff - 2;
            } else {
                *$++ = 0xe9;
                i4(diff - 5);
            }
        } else {
            if (!force_near_jmp && diff >= -126 && diff <= 129) {
                *$++ = 0x70 | j;
                *$++ = (i8) diff - 2;
            } else {
                i<0x0f, 0x80 | j>();
                i4(diff - 6);
            }
        }
    }

    void preamble() {
        i<0xf3, 0x0f, 0x1e, 0xfa>(); /// endbr64
        i<0x48, 0x8b, 0x37>();       /// mov rsi, [rdi]   ; start
        i<0x48, 0x8b, 0x57, 0x08>(); /// mov rdx, [rdi+8] ; end
        i<0x53>();                   /// push rbx
        i<0xeb, 0x18>();             /// jmp .after

        rej_addr = $;
        i<0x48, 0x31, 0xc0>(); /// reject: xor rax, rax
        i<0x5b>();             /// pop rbx
        i<0xc3>();             /// ret

        acc_addr = $;
        i<0xb8, 0x01, 0x00, 0x00, 0x00>(); /// accept: mov eax, 1
        i<0x44, 0x89, 0x57, 0x20>();       /// mov dword [rdi+32], r10d ; cap_idx
        i<0x4c, 0x89, 0x4f, 0x10>();       /// mov [rdi+16], r9         ; cap_start
        i<0x4c, 0x89, 0x5f, 0x18>();       /// mov [rdi+24], r11        ; cap_end
        i<0x5b>();                         /// pop rbx
        i<0xc3>();                         /// ret
                                           /// .after:
    }

    void create_discard() {
        i<0x48, 0x8d, 0x46, 0x04>(); /// lea rax, [rsi + 4]
        i<0x48, 0x39, 0xd6>();       /// cmp rsi, rdx
        i<0x48, 0x0f, 0x45, 0xf0>(); /// cmovne rsi, rax
    };

    static constexpr u64 opt_size(u8 class_sz) {
        switch (class_sz) {
            case 1: return 5;
            case 2: return 13;
            case 3: return 28;
            case 4: return 36;
            default: throw "Invalid class size";
        }
    }

    template <u8 class_sz, bool capture = false>
    void match_class_impl(const u8*& ip) {
        static_assert(class_sz >= 1 && class_sz <= 4);

        /// The number of bytes that are needed to match an option.
        static constexpr u64 opt_sz = opt_size(class_sz);

        /// The number of options.
        auto            sz              = READ_ADDR();
        const u8* const interm_end_addr = ip + sz * sizeof(char32_t) * class_sz;

        /// Make sure we have enough characters available.
        /// If that is not the case, we need to jmp to .reject.
        i<0x48, 0x89, 0xd0>();           /// mov rax, rdx
        i<0x48, 0x29, 0xf0>();           /// sub rax, rsi
        i<0x48, 0x83, 0xf8, class_sz>(); /// cmp rax, class_sz
        jmp<jmp_lt>(rej_addr);           /// jl .reject

        /// Load the characters to compare with.
        if constexpr (class_sz == 1) {
            i<0x8b, 0x06>(); /// mov eax, dword [rsi]
        } else if constexpr (class_sz == 2) {
            i<0x48, 0x8b, 0x06>(); /// mov rax, qword [rsi]
        } else if constexpr (class_sz == 3) {
            i<0x8b, 0x06>();             /// mov eax, dword [rsi]
            i<0x48, 0x8b, 0x5e, 0x04>(); /// mov rbx, qword [rsi + 4]
        } else if constexpr (class_sz == 4) {
            i<0x48, 0x8b, 0x06>();       /// mov rax, qword [rsi]
            i<0x48, 0x8b, 0x5e, 0x08>(); /// mov rbx, qword [rsi + 8]
        }

        /// Determine where to jump to once we find an option that matches.
        /// Each option is opt_sz bytes + 6 for the jump + another 6 if we're capturing.
        /// Additionally, we need to add another 5 bytes once for a jmp to .reject
        /// if nothing matches.
        u8* bc_end_addr = $ + 5 + (opt_sz + 6 + 6 * capture) * sz;

        /// Perform the comparison.
        u32 idx{};
        while (ip < interm_end_addr) {
            /// Save the capture index.
            if constexpr (capture) {
                /// mov r10d, idx
                i<0x41, 0xba>();
                i4(idx);
            }

            if constexpr (class_sz == 1) {
                /// cmp eax, c
                char32_t c = ip32(0);
                i<0x3d>();
                i4(c);
            } else if constexpr (class_sz == 2) {
                /// mov r8, (c << 32) | c2
                char32_t c = ip32(0), c2 = ip32(1);
                i<0x49, 0xb8>();
                i4(c);
                i4(c2);

                /// cmp rax, r8
                i<0x4c, 0x39, 0xc0>();
            } else if constexpr (class_sz == 3) {
                /// cmp eax, c
                char32_t c = ip32(0);
                i<0x3d>();
                i4(c);

                /// setnz cl
                i<0x0f, 0x95, 0xc1>();

                /// mov r8, (c2 << 32) | c3
                char32_t c2 = ip32(1), c3 = ip32(2);
                i<0x49, 0xb8>();
                i4(c2);
                i4(c3);

                i<0x4c, 0x39, 0xc3>();       /// cmp rbx, r8
                i<0x41, 0x0f, 0x95, 0xc0>(); /// setnz r8b
                i<0x44, 0x08, 0xc1>();       /// or cl, r8b
            } else if constexpr (class_sz == 4) {
                /// Load four characters.
                char32_t c = ip32(0), c2 = ip32(1), c3 = ip32(2), c4 = ip32(3);

                /// Store them in rcx and r8.
                /// mov rcx, (c << 32) | c2
                i<0x48, 0xb9>();
                i4(c);
                i4(c2);

                /// mov r8, (c3 << 32) | c4
                i<0x49, 0xb8>();
                i4(c3);
                i4(c4);

                /// Compare them.
                i<0x48, 0x39, 0xc8>();       /// cmp rax, rcx
                i<0x0f, 0x95, 0xc1>();       /// setnz cl
                i<0x4c, 0x39, 0xc3>();       /// cmp rbx, r8
                i<0x41, 0x0f, 0x95, 0xc0>(); /// setnz r8b
                i<0x44, 0x08, 0xc1>();       /// or cl, r8b
            }

            /// jz .end
            i<0x0f, 0x84>();
            i4(u32(bc_end_addr - $) - 4);

            ip += 4 * class_sz;
            idx++;
        }

        /// jmp .reject
        jmp<jmp_jmp, true>(rej_addr);

        /// Make sure we did this right.
        ASSERT($ == bc_end_addr, "Incorrect address calculation, expected to be {:08X}, was {:08X}",
            u64(bc_end_addr), u64($));
        ip = interm_end_addr;

        /// Save the start and end of the capture and advance by the correct number of characters.
        if constexpr (capture) i<0x49, 0x89, 0xf1>(); /// mov r9, rsi
        i<0x48, 0x83, 0xc6, 0x04 * class_sz>();       /// add rsi, 4 * class_sz
        if constexpr (capture) i<0x49, 0x89, 0xf3>(); /// mov r11, rsi
    }

    void compile_machine(const u8* const _data) {
        const u8* ip = _data;
        for (;;) {
            switch (auto opcode = *ip++; opcode) {
                case fsm::i_one: {
                    auto addr = READ_ADDR();
                    i<0x81, 0x3e>(); /// cmp dword [rsi], c
                    i4(addr);
                    jmp<jmp_ne>(rej_addr); /// jne .reject
                    create_discard();      /// discard()
                    break;
                }

                case fsm::i_any: {
                    i<0x48, 0x39, 0xd6>();       /// cmp rsi, rdx
                    jmp<jmp_eq>(rej_addr);       /// je .reject
                    i<0x48, 0x83, 0xc6, 0x04>(); /// add rsi, 4
                    break;
                }

                case fsm::i_capture_start: {
                    auto capture_idx = READ_ADDR();

                    /// capture_index = capture_idx
                    i<0x41, 0xba>(); /// mov r10d, idx
                    i4(capture_idx);

                    /// cap_start	  = curr
                    i<0x49, 0x89, 0xf1>(); /// mov r9, rsi
                    break;
                }

                case fsm::i_capture_end: {
                    /// cap_start	  = curr
                    i<0x49, 0x89, 0xf3>(); /// mov r11, rsi
                    break;
                }

                case fsm::i_accept: {
                    jmp<jmp_jmp>(acc_addr);
                    return;
                }

                case fsm::i_reject: {
                    jmp<jmp_jmp>(rej_addr);
                    return;
                }

                case fsm::i_opt: {
                    auto skip_addr = READ_ADDR();

                    /// SAVE()
                    i<0x56>(); /// push rsi
                    auto _acc_addr = acc_addr;
                    auto _rej_addr = rej_addr;

                    /// Compile the submachine. Skip it irrespective of whether it returns or not.
                    /// To do that, we allocate a jmp instruction here whose target address we'll
                    /// fill in after we're done emitting the submachine.
                    i<0xeb, 0x05>(); /// Skip the jmp instruction that skips the submachine.
                    auto jmp_instr = acc_addr = rej_addr = $;
                    i<0xe9>();

                    /// Allocate 4 bytes for later
                    $ += 4;

                    /// Compile the submachine
                    compile_machine(ip);

                    /// Fill in the jmp to skip the machine and reset the return addresses.
                    fixup_near_jmp(jmp_instr);
                    acc_addr = _acc_addr;
                    rej_addr = _rej_addr;

                    /// RESTORE()
                    i<0x5e>(); /// pop rsi

                    /// Continue compiling after the machine.
                    ip = _data + skip_addr;
                    break;
                }

                case fsm::i_neg: {
                    auto skip_addr = READ_ADDR();

                    /// Reject if this matches.
                    auto _acc_addr = acc_addr;
                    auto _rej_addr = rej_addr;
                    acc_addr       = _rej_addr;

                    /// Allocate a jmp instruction to jump over the submachine.
                    i<0xeb, 0x05>(); /// Skip the jmp instruction that skips the submachine.
                    auto jmp_instr = rej_addr = $;
                    i<0xe9>();

                    /// Fill in the jmp to skip the machine and reset the return addresses.
                    fixup_near_jmp(jmp_instr);
                    acc_addr = _acc_addr;
                    rej_addr = _rej_addr;

                    /// Continue compiling after the machine.
                    ip = _data + skip_addr;
                    break;
                }

                case fsm::i_try: {
                    /// Read the end address.
                    auto success_addr = READ_ADDR();

                    /// We'll need to reset these later.
                    auto _acc_addr = acc_addr;
                    auto _rej_addr = rej_addr;

                    /// Allocate a jmp instruction to jump to the end.
                    i<0xeb, 0x05>(); /// Skip the jmp instruction.
                    auto jmp_instr = $;
                    i<0xe9>();

                    /// Compile the options.
                    for (;;) {
                        /// Read the address that indicates the start of the next option
                        auto next_addr = READ_ADDR();
                        acc_addr       = jmp_instr;

                        /// If this is the last option, then we reject on failure.
                        u8* next_opt_instr;
                        if (next_addr == FSM_TRY_REJECT) rej_addr = _rej_addr;

                        /// Otherwise, allocate a jmp instruction to jump to the next address.
                        else {
                            i<0xeb, 0x05>(); /// Skip the jmp instruction.
                            next_opt_instr = rej_addr = $;
                            i<0xe9>();
                        }

                        /// SAVE()
                        i<0x56>(); /// push rsi

                        /// Execute the submachine and jump to the end of this try-sequence if it accepts.
                        compile_machine(ip);

                        /// If this was the last option, skip to after the try.
                        if (next_addr == FSM_TRY_REJECT) {
                            ip = _data + success_addr;
                            fixup_near_jmp(jmp_instr);
                            acc_addr = _acc_addr;
                            rej_addr = _rej_addr;
                            break;
                        }

                        /// Otherwise, skip to the next option and fill in the
                        /// jmp instruction we just allocated.
                        else {
                            ip = _data + next_addr;
                            fixup_near_jmp(next_opt_instr);
                        }

                        /// RESTORE()
                        i<0x5e>(); /// pop rsi
                    }
                    break;
                }

                case fsm::i_boundary: {
                    i<0x48, 0x8b, 0x07>();       /// mov rax, [rdi]
                    i<0x48, 0x39, 0xc6>();       /// cmp rsi, rax
                    i<0x41, 0x0f, 0x94, 0xc0>(); /// sete r8b
                    i<0x48, 0x39, 0xd6>();       /// cmp rsi, rdx
                    i<0x0f, 0x94, 0xc1>();       /// sete cl
                    i<0x44, 0x08, 0xc1>();       /// or cl, r8b
                    jmp<jmp_ne>(acc_addr);       /// jnz .accept
                    jmp<jmp_jmp>(rej_addr);      /// jmp .reject
                    break;
                }

                case fsm::i_match_class: match_class_impl<1, false>(ip); break;
                case fsm::i_match_class_2: match_class_impl<2, false>(ip); break;
                case fsm::i_match_class_3: match_class_impl<3, false>(ip); break;
                case fsm::i_match_class_4: match_class_impl<4, false>(ip); break;

                case fsm::i_match_capture: match_class_impl<1, true>(ip); break;
                case fsm::i_match_capture_2: match_class_impl<2, true>(ip); break;
                case fsm::i_match_capture_4: match_class_impl<3, true>(ip); break;
                case fsm::i_match_capture_3: match_class_impl<4, true>(ip); break;

                default: throw compiler::error(fmt::format("Invalid instruction: {:X}", u32(*ip)));
            }
        }
    };
};

auto compiler::compile_to_native(const parser& p, bool dump_ir) -> std::vector<fsm> {
    /// Compile the rules to intermediate code.
    auto fsms = compile(p);

    /// Compile the intermediate code to native machine code.
    for (u64 i = 0; i < fsms.size(); i++) {
        /// Allocate a function.
        void* const mem = mmap(nullptr, 4096, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
        if (mem == MAP_FAILED) throw compiler::error(fmt::format("Failed to allocate memory for compiled function: {}", std::strerror(errno)));
        fsms[i].native_handle = reinterpret_cast<native_match_fun>(mem);
        code_generator cg(static_cast<uint8_t*>(mem));

        /// Compile the intermediate code.
        cg.preamble();
        cg.compile_machine(fsms[i].bytecode.data());

        /// For testing and debugging.
        /*auto f = fopen("test.bin", "w");
        fwrite(cg.mem, 1, cg.$ - cg.mem, f);
        fclose(f);
        std::exit(0);*/
    }
    return fsms;
}
#endif

#define SAVE()       auto _curr = curr;
#define RESTORE()    curr = _curr;
#define SET_IP(addr) ip = machine.bytecode.data() + (addr)

#define MATCH_CLASS_IMPL(class_sz, cond, ...)                             \
    do {                                                                  \
        /** Read the size of this class. **/                              \
        const u32 sz = ip32(0);                                           \
        ip += sizeof(u32);                                                \
                                                                          \
        /** Compute the end address. **/                                  \
        const u8* end_addr = ip + sz * sizeof(char32_t) * class_sz;       \
                                                                          \
        /** Make sure we have at least class_sz characters. **/           \
        if (u64(end - curr) < class_sz) return false;                     \
                                                                          \
        /** Index of the element we're matching. **/                      \
        __VA_OPT__(u32 cap_idx = 0;)                                      \
                                                                          \
        while (ip < end_addr) {                                           \
            if (cond) {                                                   \
                /** If we're matching a capture, record its location. **/ \
                __VA_OPT__(                                               \
                    capture_index = cap_idx;                              \
                    cap_start     = curr;                                 \
                    cap_end       = curr + class_sz; /**/                 \
                )                                                         \
                                                                          \
                /** Go to the next instruction. **/                       \
                ip = end_addr;                                            \
                discard();                                                \
                goto next;                                                \
            }                                                             \
            ip += sizeof(char32_t) * class_sz;                            \
            __VA_OPT__(cap_idx++;)                                        \
        }                                                                 \
        return false;                                                     \
    } while (0)

auto match_context::do_match() RELEASE_NOEXCEPT -> bool {
    for (;;) {
    next:
        switch (*ip++) {
            case fsm::i_one: {
                if (*curr != ip32(0)) return false;
                discard();
                ip += 4;
                continue;
            }

            case fsm::i_any: {
                if (curr == end) return false;
                discard();
                continue;
            }

            case fsm::i_opt: {
                auto skip_addr = READ_ADDR();
                SAVE();
                /// Skip the machine if it rejects
                if (!do_match()) {
                    SET_IP(skip_addr);
                    RESTORE();
                }
                continue;
            }

            case fsm::i_try: {
                /// Read the end address
                auto success_addr = READ_ADDR();
                for (;;) {
                    /// Read the address that indicates the start of the next option
                    auto next_addr = READ_ADDR();
                    SAVE();
                    /// Execute the submachine and jump to the end of this try-sequence if it accepts
                    if (do_match()) {
                        SET_IP(success_addr);
                        break;
                    }
                    /// Otherwise, move on to the next option, or reject if this was the last option
                    if (next_addr == FSM_TRY_REJECT) return false;
                    RESTORE();
                    SET_IP(next_addr);
                }
                continue;
            }

            case fsm::i_match_class: MATCH_CLASS_IMPL(1, curr[0] == ip32(0));
            case fsm::i_match_class_2: MATCH_CLASS_IMPL(2, curr[0] == ip32(0) && curr[1] == ip32(1));
            case fsm::i_match_class_3: MATCH_CLASS_IMPL(3, curr[0] == ip32(0) && curr[1] == ip32(1) && curr[2] == ip32(2));
            case fsm::i_match_class_4: MATCH_CLASS_IMPL(4, curr[0] == ip32(0) && curr[1] == ip32(1) && curr[2] == ip32(2) && curr[3] == ip32(3));

            case fsm::i_match_capture: MATCH_CLASS_IMPL(1, curr[0] == ip32(0), true);
            case fsm::i_match_capture_2: MATCH_CLASS_IMPL(2, curr[0] == ip32(0) && curr[1] == ip32(1), true);
            case fsm::i_match_capture_3: MATCH_CLASS_IMPL(3, curr[0] == ip32(0) && curr[1] == ip32(1) && curr[2] == ip32(2), true);
            case fsm::i_match_capture_4: MATCH_CLASS_IMPL(4, curr[0] == ip32(0) && curr[1] == ip32(1) && curr[2] == ip32(2) && curr[3] == ip32(3), true);

            case fsm::i_neg: {
                auto skip_addr = READ_ADDR();
                if (!do_match()) SET_IP(skip_addr);
                else return false;
                continue;
            }

            case fsm::i_capture_start: {
                auto capture_idx = READ_ADDR();
                capture_index    = capture_idx;
                cap_start        = curr;
                continue;
            }

            case fsm::i_capture_end: {
                cap_end = curr;
                continue;
            }

            case fsm::i_boundary: {
                if (curr == end || curr == w.data.data()) continue;
                return false;
            }

            case fsm::i_accept: {
                return true;
            }

            case fsm::i_reject: {
                return false;
            }

            default:
#ifndef NDEBUG
                machine.dump();
                throw compiler::error(fmt::format("Invalid instruction: {:X}", u32(*ip)));
#else
                return false;
#endif
        }
    }
}
#undef MATCH_CLASS_IMPL
#undef ip32
#undef SET_IP
#undef READ_ADDR
#undef RESTORE
#undef SAVE

} // namespace sca
