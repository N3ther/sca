#include "../3rdparty/fmt/include/fmt/format.h"
#include "../lib/parser.hh"
#include "../lib/applier.hh"

using namespace sca;

enum struct test_result {
    ok,
    warning,
    error,
};

u64         run, failed, errored;
std::string test_stage;
std::string msg;

template <>
struct fmt::formatter<std::vector<parser::element>> {
    template <typename ctx>
    constexpr auto parse(ctx& c) { return c.begin(); }

    template <typename ctx>
    auto format(const std::vector<parser::element>& r, ctx& c) {
        std::u32string s;
        for (u64 i = 0; i < r.size(); i++) {
            s += U"\n";
            s += r[i].str(3);
        }
        return fmt::format_to(c.out(), "    (elements{})", to_utf8(s));
    }
};

template <typename... args_t>
void report_assertion_failed(int line,
    const std::string&           input,
    const std::string&           cond,
    const std::string&           msg,
    bool                         raised_exception,
    args_t... args) {
    fmt::print(stderr, "\033[31m[Line {}] Test {}\n"
                       "    \033[0;33mTesting\033[34m   {}\n"
                       "    \033[33mInput\033[32m     \"{}\"\n"
                       "    \033[33mFailed to\033[34m {}\033[m\n"
                       "{}\n",
        line, raised_exception ? "raised exception" : "failed",
        test_stage, json_escape(input), cond, msg);
}

void rawtest(const std::string& description,
    const std::string&          cond,
    std::function<bool()>       test_proc,
    test_result                 res  = test_result::ok,
    int                         line = __builtin_LINE()) try {
    test_result o = test_result::ok;
    msg.clear();
    run++;
    if (!test_proc()) o = test_result::error;
    if (o != res) {
        failed++;
        report_assertion_failed(line, cond, description, msg, false);
    }
} catch (const std::runtime_error& e) {
    errored++;
    report_assertion_failed(line, cond, description, e.what(), true);
} catch (...) {
    report_assertion_failed(line, cond, description, "", true);
}

void test(parser&                p,
    const std::string&           description,
    const std::u32string&        input,
    std::function<bool(parser&)> test_proc,
    test_result                  res  = test_result::ok,
    int                          line = __builtin_LINE()) try {
    test_result o = test_result::ok;
    msg.clear();
    run++;
    p.diagnostic_handler = [&](parser::diagnostic&& d) {
        msg += d.message();
        if (d.level == parser::diagnostic_level::warning && o == test_result::ok)
            o = test_result::warning;
    };
    try {
        p.init(input);
        if (!test_proc(p)) o = test_result::error;
        if (o != res) {
            failed++;
            report_assertion_failed(line, to_utf8(input), description, msg, false);
        }
    } catch (const assertion_error& e) {
        throw assertion_error(e, parser::diagnostic{&p, {p.loc, p.loc}, parser::diagnostic_level::error, ""}.line());
    }
} catch (const std::runtime_error& e) {
    errored++;
    report_assertion_failed(line, to_utf8(input), description, e.what(), true);
} catch (...) {
    report_assertion_failed(line, to_utf8(input), description, "", true);
}

void test(const std::string&     description,
    const std::u32string&        input,
    std::function<bool(parser&)> test_proc,
    test_result                  res  = test_result::ok,
    int                          line = __builtin_LINE()) {
    parser p;
    test(p, description, input, test_proc, res, line);
}

void test_tok(const std::string& description,
    const std::u32string&        input,
    parser::token_type           t,
    test_result                  res           = test_result::ok,
    bool                         parsing_rule  = false,
    bool                         consume_first = false,
    int                          line          = __builtin_LINE()) { // clang-format off
	test(description, input, [=](auto& p){
		p.parsing_rule = parsing_rule;
		if (consume_first) p.next_token();
		return p.tok.type == t;
	}, res, line); // clang-format on
}

void test_rule(const std::string& description,
    const std::u32string&         input,
    bool (parser::*parse_fun)(),
    parser::rule_type t,
    test_result       res  = test_result::ok,
    int               line = __builtin_LINE()) { // clang-format off
	test(description, input, [=](auto& p){
		return (p.*parse_fun)() && p.rules[0].type == t;
	}, res, line); // clang-format on
}

void test_rule_w_input(const std::string& description,
    const std::u32string&                 input,
    bool (parser::*parse_fun)(parser::element&&),
    parser::rule_type t,
    test_result       res  = test_result::ok,
    int               line = __builtin_LINE()) { // clang-format off
	test(description, input, [=](auto& p){
		auto [el, success] = p.parse_input();
		return success && p.consume(parser::token_type::separator) && (p.*parse_fun)(std::move(el));
	}, res, line); // clang-format on
}

auto test_element(parser& p,
    const std::string&    description,
    const std::u32string& input,
    parser::parse_result<parser::element> (parser::*test_proc)(),
    test_result res  = test_result::ok,
    int         line = __builtin_LINE()) -> parser::parse_result<parser::element> {
    parser::parse_result<parser::element> result{false};
    test(
        p, description, input, [&](auto& p) {
            auto [e, s] = (p.*test_proc)();
            if (s) result = std::move(e);
            return s;
        },
        res, line);
    return result;
}

auto test_element(const std::string& description,
    const std::u32string&            input,
    parser::parse_result<parser::element> (parser::*test_proc)(),
    test_result res  = test_result::ok,
    int         line = __builtin_LINE()) -> parser::parse_result<parser::element> {
    parser p;
    return test_element(p, description, input, test_proc, res, line);
}

void test_classes_eq(const std::string& description,
    const std::vector<parser::element>& actual,
    const std::vector<parser::element>& expected,
    test_result                         res  = test_result::ok,
    int                                 line = __builtin_LINE()) {
    parser::parse_result<parser::element> result{false};
    rawtest(
        description, "(see below)", [&] {
            bool res = actual == expected;
            if (!res) msg = fmt::format("    \033[33mElements:\033[31m\n{}\n"
                                        "    \033[33mExpected:\033[32m\n{}\033[m",
                          actual, expected);
            return res;
        },
        res, line);
}

auto test_parses(const std::string& description,
    const std::u32string&           input,
    test_result                     res  = test_result::ok,
    int                             line = __builtin_LINE()) -> bool {
    std::u32string class_input =
        U"V={a,e,i,o,u}\n"
        U"L={ā,ē,ī,ō,ū}\n"
        U"C={p,t,c,q,b,d,g,m,n,l,r,h,s}\n"
        U"F={i,e}\n"
        U"B={o,u}\n"
        U"S={p,t,c}\n"
        U"Z={b,d,g}";
    parser p;
    ASSERT(p.parse_classes(class_input));
    test(
        p, description, input, [&](auto& p) { (void) p.parse_rule(); return !p.has_error; }, res, line);
    return !p.has_error;
}

#define DO(...) [&](auto& p) { __VA_ARGS__; }

parser::element make_el(const std::u32string& text, parser& p) {
    p.init(text);
    ASSERT(p.tok.type == parser::token_type::text, "");
    return parser::element::from_curr_text_tok(p);
}

parser::element make_el(const std::vector<parser::element>& els, parser&) {
    parser::element el;
    el.type = parser::element_type::cclass;
    el.els  = els;
    return el;
}

template <typename... args_t>
std::vector<parser::element> el_vec(args_t... args) {
    parser                       p;
    std::vector<parser::element> els;
    ((els.push_back(make_el(args, p))), ...);
    return els;
}

/// For debugging.
bool l(parser& p) {
    using poss [[maybe_unused]] = std::vector<word::char_pos>;
    asm volatile("int $3;");
    return false;
}

void agma_tests() {
    static std::string ok  = "parse a well-formed sound change";
    static std::string nok = "report an error when encountering an ill-formed sound change";
    static std::string r   = "report an error when encountering";

    test_stage = "Parser: Miscellaneous sound changes";
    test_parses(ok, U"tt/t/t_t");
    test_parses(ok, U"t//t_t");
    test_parses(ok, U"{a,e,i}/{ha,he,hi}/#_~[{a,e,i,h}]");
    test_parses(ok, U"{C,V}/+/_{V,C}");
    test_parses(ok, U"{cheese, milk}/{lemon}/cheese_milk");
    test_parses(ok, U"!?/!!/!_!");
    test_parses(ok, U"{{a,e,i},{o,u}}/{{o,u,y},{a,e}}/C_C");
    test_parses(ok, U"a/%{20%e,40%i,40%y}/j_");
    test_parses(ok, U"ae/&/e_a");
    test_parses(ok, U"ng/g/n_");
    test_parses(ok, U"ng/n/_g");
    test_parses(ok, U"p/f/#_V#");
    test_parses(ok, U"ql/l/{z,h,g}_{C,N,S, a, e, i}");
    test_parses(ok, U"t,tt,ttt/t/C~t_");
    test_parses(ok, U"H/Hh/_");
    test_parses(ok, U"h/Hh/_");
    test_parses(ok, U"t,s,h//_#");
    test_parses(ok, U"taco/TACO/ta_co");
    test_parses(ok, U"{ta,t,a}/{se,s,e}/_l");
    test_parses(ok, U"ve//ve_");
    test_parses(ok, U"bub/&/_b");
    test_parses(ok, U"jjj/jj/j_~[j]");
    test_parses(ok, U" oOoO/&/_");
    test_parses(ok, U" oOoO/OoOo/_");
    test_parses(ok, U" OOoo/%{50%OoOo,50%oOoO}/_");

    test_parses("warn on nop ~", U"{C~c,V~v,o,v,s}/g/_", test_result::warning);
    test_parses("warn on nop ~", U"C/{C}s/_C~N#", test_result::warning);
    test_parses("warn on pointless use of ~", U"{V~{t,s}~s,C~{a,e}~e}/j/_", test_result::warning);
    test_parses("warn on pointless use of ~", U"C~N/t/_#", test_result::warning);

    test_parses(nok, U"z/z+/V_V", test_result::error);
    test_parses(nok, U"h/*/V~*_", test_result::error);
    test_parses(nok, U"/+/_", test_result::error);
    test_parses(nok, U"/&/_", test_result::error);
    test_parses(nok, U"/*/_", test_result::error);
    test_parses(nok, U"g/k/{,t,d}_", test_result::error);
    test_parses(nok, U"{,h,k}/+/_{h,k,}", test_result::error);
    test_parses(nok, U"{,,,}/h/#_", test_result::error);
    test_parses(nok, U"a/e/V_{,,,}", test_result::error);
    test_parses(nok, U"a/i/%{20%j,80%w}_", test_result::error);
    test_parses(r + "a subst rule with more inputs than outputs", U"{C, c}/{V, v} {C,V}_{c,v}", test_result::error);
    test_parses(r + "a subst rule with fewer inputs than outputs", U" C~C~C~c/G/~c_~c", test_result::error);
    test_parses("report an error if the lhs of ~ is not a class", U"{C~c,V~v,O~o,v,s}/g/_", test_result::error);
    test_parses(nok, U"A~a,V~{v,V~v}/h/_", test_result::error);
    test_parses(nok, U"t/{20%+,80%++}/V_V", test_result::error);
    test_parses(nok, U"ts/{20%&,80%tts}/V_V", test_result::error);
    test_parses(nok, U"g/*/V_#", test_result::error);
    test_parses(nok, U"/++/V_", test_result::error);
    test_parses(nok, U"{A,B}/{+,&}/_", test_result::error);
    test_parses(nok, U"{n,ng}/{n+,ng&}/_", test_result::error);
    test_parses(nok, U"o/o*/_", test_result::error);
    test_parses(nok, U"o/*+/_", test_result::error);
    test_parses(nok, U"ol/&+/_", test_result::error);
    test_parses(nok, U"{o,n,o}/%{90%o,90%n,91%o}_", test_result::error);
    test_parses(nok, U" OOoo/%{50%OoOo,50%oOoO}_", test_result::error);
    test_parses(r + " a subst rule with more inputs than outputs", U" {C}{V}C/{C}V/_", test_result::error);
    test_parses(r + " a subst rule with more inputs than outputs", U" {C}{C}C/{C}C/_~C", test_result::error);
    test_parses(nok, U"j/{1%+,99%}/_", test_result::error);
    test_parses(nok, U"ing/{1%&,2%+,97%}/_", test_result::error);
    test_parses(nok, U"{k,g,h}~g/{g,w,k}/_", test_result::error);
    test_parses(nok, U"{k,g,h}/{g,w,k}~w/_", test_result::error);
    test_parses(nok, U"o/C~N/_", test_result::error);
    test_parses(nok, U"*//", test_result::error);

    {
        std::u32string cl_input = U"C ={p,t,k,b,d,g,m,n,s,w,l,j,r,z,x,c,v,h,q}\nV = {a,e,i,o,u,y}\nN = {m,n}\nK = {p,t,k,q}\nG = {b,d,g,q}\nF = {f,t,x,h}\nZ = {v,d,j,h}";
        std::u32string w_input  = U"ha\ni\na\neigak\ny\ngy\nxu\nwe\namvoben\nymuan\nkomjanom\nsi\nqan\nlo\nze\npunm\no\ne\nqams\nleq\nu\nty\nle\ntu\nba\nbe\nin\nju\nko\ngu\nsaymwynx\nnunpoim\nsyiqenc\nag\nhe\nenz\nbemd\nhu\nhy\njamiek\nyumyl\nvynsynuk\nenq\ngunain\noyntuq\nso\nzo\nto\ngi\nvo\nxamkm\naua\nza\ninr\numromy\nymynzi\nouir\nmonz\noeynt\nga\nziowy\nzameaw\njymkv\nxe\nynimup\nkot\noumton\numimhimm\nzomaymj\nwinongin\nonevu\nwymannyj\ndeynkun\nnu\niya\nonzynem\ncuemlat\ndem\nianon\nlu\nkyigimx\nwy\nry\npi\nez\njomqs\nann\nwiyyw\nlax\nxonenin\nzi\nyn\nont\nomlumrem\ndy\nymlanu\numcoa\nzenumbymj\nmyunom\nlyumsa\ncy\nzyh\nuimim\nqinavid\nwi\nmy\nbomudov\naneum\njy\nbenkanqams\nvu\nxumzynynt\nimezonw\nanr\ninvemomr\nxa\nta\nveimemb\neq\namimhe\nanx\nziamre\ncimh\nwaukaq\nqomvysynp\nzu\nmu\ndaurom\nvy\n";
        std::u32string r_input  = U"h/x/_#\n"
                                 U"p/b/V_V\n"
                                 U"p,b/f,f/#_\n"
                                 U"n//V_m#\n"
                                 U"F/Z/_V~{a,e,i}\n"
                                 U"C/+/V~{a,e,i}_V~{o,u,y}\n"
                                 U"{m,n}/+/V_V\n"
                                 U"/h/#V_#\n"
                                 U"G/K/_#\n"
                                 U"u/y/_#\n"
                                 U"V//V_\n"
                                 U"V/+/C_{C}{V}\n"
                                 U"V/+/C_{C}{C}\n"
                                 U"{C}{V}/&/#_#\n"
                                 U"mw/&/V_V\n"
                                 U"qan/kan/_\n"
                                 U"C//{V}{V}{C}_\n"
                                 U"yy/y/_\n"
                                 U"le/lel/#_#\n"
                                 U"G+G/K+K/_#\n"
                                 U"Z/F/_*~[V]\n"
                                 U"q/kl/V_*";
        parser p;
        auto [words, success] = p.parse_words(w_input);
        ASSERT(p.parse_classes(cl_input));
        ASSERT(p.parse_rules(r_input));
        ASSERT(success);

        /// TODO:
        applier a{p};
        for (u64 i = 0 ; i < a.rules.size(); i++)
            for (const auto& w : words);
                // rawtest(fmt::format("Apply rule {} to {} correctly", i, to_utf8(w.data)), fmt::format(""))
    }
}

int main() {
    using enum parser::token_type;
    using enum parser::rule_type;
    using enum test_result;

    using poss [[maybe_unused]]    = std::vector<word::char_pos>;
    using element [[maybe_unused]] = parser::element;

    ///
    /// Lexer Tests
    ///

    /// Text
    test_stage = "Lexer: Text Tokens";
    test_tok("parse a text token", U"foobar", text);
    test_tok("parse a text token containing characters consisting of multiple utf-8 codepoints", U"桜桜桜桜", text);
    test_tok("parse a text token containing diacritics", U"t̶̨̏e̷͚͝s̵͙̓t̴̠͊", text);
    test_tok("parse a text token consisting of a number", U"202222222222222222222222222222222222222222222", text);

    /// Diacritics
    test("split a simple word", U"foobar", DO(return p.tok.text.chars == poss{{0, 1}, {1, 1}, {2, 1}, {3, 1}, {4, 1}, {5, 1}}));
    test("reverse a simple word", U"foobar", DO(return p.tok.text.reverse() == U"raboof"));

    test("split a word containing multibyte characters", U"桜桜櫻櫻", DO(return p.tok.text.chars == poss{{0, 1}, {1, 1}, {2, 1}, {3, 1}}));
    test("reverse a word containing multibyte characters", U"桜桜櫻櫻", DO(return p.tok.text.reverse() == U"櫻櫻桜桜"));

    test("split zalgo text", U"t̶̨̏e̷͚͝s̵͙̓t̴̠͊", DO(return p.tok.text.chars == poss{{0, 4}, {4, 4}, {8, 4}, {12, 4}}));
    test("reverse zalgo text", U"t̶̨̏e̷͚͝s̵͙̓t̴̠͊", DO(return p.tok.text.reverse() == U"t̴̠͊s̵͙̓e̷͚͝t̶̨̏"));

    test("split text containing modifier letters", U"tʰʰˢʷ̣̣eʰʰ", DO(return p.tok.text.chars == poss{{0, 7}, {7, 3}}));
    test("reverse text containing modifier letters", U"tʰʰˢʷ̣̣eʰʰ", DO(return p.tok.text.reverse() == U"eʰʰtʰʰˢʷ̣̣"));

    test("split text containing a tie bar", U"at͡ʃb", DO(return p.tok.text.chars == poss{{0, 1}, {1, 3}, {4, 1}}));
    test("reverse text containing a tie bar", U"at͡ʃb", DO(return p.tok.text.reverse() == U"bt͡ʃa"));

    test("split text containing multiple tie bars", U"t͡ʃ͡s͡vbs͡͡s͡e", DO(return p.tok.text.chars == poss{{0, 7}, {7, 1}, {8, 6}}));
    test("reverse text containing multiple tie bars", U"t͡ʃ͡s͡vbs͡͡s͡e", DO(return p.tok.text.reverse() == U"s͡͡s͡ebt͡ʃ͡s͡v"));

    test("not treat special characters as letters after a tie bar", U"t͡&", DO(return p.tok.text.data == U"t͡"));
    test("not treat whitespace as letters after a tie bar", U"t͡ ", DO(return p.tok.text.data == U"t͡"));

    /// Line breaks
    test_stage = "Lexer: Line Breaks";
    test_tok("ignore \n when not parsing a rule", U"\n", end_of_string);
    test_tok("ignore \r when not parsing a rule", U"\r", end_of_string);
    test_tok("ignore spaces", U"      ", end_of_string);
    test_tok("parse \n as newline when parsing a rule", U"q\n", newline, ok, true, true);
    test_tok("parse \r\n as newline when parsing a rule", U"q\r\n", newline, ok, true, true);
    test_tok("parse \n\r as newline when parsing a rule", U"q\n\r", newline, ok, true, true);
    test_tok("parse \r as newline when parsing a rule", U"q\r", newline, ok, true, true);
    test_tok("parse \n as newline when parsing a rule", U"\n", end_of_string, ok, true, true);
    test_tok("parse \r\n as newline when parsing a rule", U"\r\n", end_of_string, ok, true, true);
    test_tok("parse \n\r as newline when parsing a rule", U"\n\r", end_of_string, ok, true, true);
    test_tok("parse \r as newline when parsing a rule", U"\r", end_of_string, ok, true, true);
    test_tok("parse \n\n as two newlines when parsing a rule", U"q\n\n", newline, ok, true, true);
    test_tok("parse \r\r as two newlines when parsing a rule", U"q\r\r", newline, ok, true, true);

    /// Tokens
    test_stage = "Lexer: Tokens";
    test_tok("parse \"/\" as separator", U"/", separator);
    test_tok("parse \"%{\" as percentage", U"%{", percentage);
    test_tok("parse \"20%\" as percentage", U"20%", percentage);
    test_tok("parse \"//\" as separator", U"//", separator);
    test_tok("parse \">\" as arrow", U">", arrow);
    test_tok("parse \"{\" as lbrace", U"{", lbrace);
    test_tok("parse \"}\" as rbrace", U"}", rbrace);
    test_tok("parse \"(\" as lparen", U"(", lparen);
    test_tok("parse \")\" as rparen", U")", rparen);
    test_tok("parse \"#\" as hash", U"#", hash);
    test_tok("parse \"$\" as  dollar", U"$", dollar);
    test_tok("parse \"&\" as ampersand", U"&", ampersand);
    test_tok("parse \"+\" as plus", U"+", plus);
    test_tok("parse \"=\" as assign", U"=", assign);
    test_tok("parse \"~\" as tilde", U"~", tilde);
    test_tok("parse \",\" as comma", U",", comma);
    test_tok("parse \"*\" as wildcard", U"*", wildcard);
    test_tok("parse \"|\" as vbar", U"|", vbar);
    test_tok("parse \"_\" as uscore", U"_", uscore);
    test_tok("parse \"____\" as uscore", U"____", uscore);
    test_tok("parse \"____\" as one uscore", U"_____", end_of_string, ok, false, true);
    test_tok("parse \"__ ___\" as two uscores", U"__ ___", uscore, ok, false, true);

    test_tok("report an error if \"%\" is not preceded by a number of followed by \"{\"", U"%", percentage, error);

    /// Tests for end of input.
    /// Keep in mind that next_token is called once when new input is loaded
    test_stage = "Lexer: End of Input";
    test_tok("set tok.type to end_of_string when passed an empty input", U"", end_of_string);
    test("report end of input when passed an empty input", U"", DO(return p.at_end));
    test("report end of input reached after parsing \"a\"", U"a", DO(return p.at_end));
    test("report end of input reached after parsing \"abcd\"", U"abcd", DO(return p.at_end));
    test("parse \"ab cd\" as two tokens", U"ab cd", DO(return !p.at_end));
    test("parse \"ab cd\" as two tokens", U"ab cd", DO(p.next_token(); return p.at_end));

    /// Words
    {
        /// Split into words
        auto              input = U"foobar\nfoo\nbar";
        std::vector<word> words;
        bool              _success;
        test(R"(parse words)", input, [&](auto p) {
            auto [ws, success] = p.parse_words(input);
            _success           = success;
            if (!success) return false;
            words = std::move(ws);
            return true;
        });
        if (!_success) goto after_words;

        rawtest("parse 3 words", "foobar\nfoo\nbar", [&] { return words.size() == 3; });
        rawtest("parse 3 words correctly", "foobar\nfoo\nbar", [&] {
            return words[0].data == U"foobar" && words[1].data == U"foo" && words[2].data == U"bar";
        });
    }
after_words:;

    ///
    /// Parser Tests
    ///

    /// <substitution-rule>  ::= <input> SEPARATOR <output>       <context>
    {
        static constexpr auto r = &parser::parse_substitution_rule;
        test_stage              = "Parser: <substitution-rule>";
        test_rule_w_input("parse a simple substitution rule", U"a/c/_", r, substitution);
        test_rule_w_input("parse a substitution rule", U"a, b/c, d/#_", r, substitution);
        test_rule_w_input("parse a substitution rule with top-level groups", U"{a, b, c}/{c, d, e}/_e", r, substitution);
        test_rule_w_input("parse a complicated substitution rule", U"s/80%{a, b, 20%c, 40%{q, 30%r, 10%{u, v, w}}, h}/_\n", r, substitution);

        test_rule_w_input("report an error if too many outputs", U"a/c, d/#_", r, substitution, error);
        test_rule_w_input("report an error if too few outputs", U"a, b, c/d, e/#_", r, substitution, error);
        test_rule_w_input("report an error if too few outputs when using percentages", U"a, b, c/d, %{e, f}/#_", r, substitution, error);
        test_rule_w_input("report an error if too many separators", U"a/c//#_", r, substitution, error);
        test_rule_w_input("report an error if too few outputs when using tl groups", U"{a, b, c}/{d, e}/_e", r, substitution, error);

        /// Test that previously raised exceptions.
        test_rule_w_input("report an error if there is a percentage within a <percent-els>",
            U"a/%{a20%c}/_\n", r, substitution, error);
    }

    /// <epenthesis-rule>    ::=         SEPARATOR <output>       <context>
    {
        static constexpr auto r = &parser::parse_epenthesis_rule;
        test_stage              = "Parser: <epenthesis-rule>";
        test_rule("parse an epenthesis rule", U"/b/_b", r, epenthesis);
        test_rule("parse an epenthesis rule with output containing percentages", U"/20%{a}/_b", r, epenthesis);
        test_rule("parse an epenthesis rule with output containing percentages", U"/%{a, b}/_b", r, epenthesis);

        test_rule("warn if output of epenthesis rule is empty", U"/b/_", r, epenthesis, warning);

        test_rule("report an error if output is empty", U"//_b", r, epenthesis, error);
        test_rule("report an error if output is \"&\"", U"/&/_b", r, epenthesis, error);
        test_rule("report an error if output is \"+\"", U"/+/_b", r, epenthesis, error);
    }

    /// <deletion-rule>      ::= <input> SEPARATOR                <context>
    {
        static constexpr auto r = &parser::parse_deletion_rule;
        test_stage              = "Parser: <deletion-rule>";
        test_rule_w_input("parse a deletion rule", U"b//_", r, deletion);
        test_rule_w_input("parse a deletion rule with multiple input elements", U"a, b, c, d//_", r, deletion);
        test_rule_w_input("parse a deletion rule with a tl class", U"{a, b, c, d}//_", r, deletion);
    }

    /// <metathesis-rule>    ::= <input> SEPARATOR "&"            <context>
    {
        static constexpr auto r = &parser::parse_metathesis_rule;
        test_stage              = "Parser: <metathesis-rule>";
        test_rule_w_input("parse a metathesis rule", U"ab/&/_", r, metathesis);
        test_rule_w_input("parse a metathesis rule with multiple input elements", U"ab, cd, ef/&/_", r, metathesis);
        test_rule_w_input("parse a metathesis rule with a tl class", U"{ab, cd, ef}/&/_", r, metathesis);
    }

    /// <reduplication-rule> ::= <input> SEPARATOR "+" { "+" }    <context>
    {
        static constexpr auto r = &parser::parse_reduplication_rule;
        test_stage              = "Parser: <reduplication-rule>";
        test_rule_w_input("parse a metathesis rule", U"ab/+/_", r, metathesis);
        test_rule_w_input("parse a metathesis rule with multiple input elements", U"ab, cd, ef/+/_", r, metathesis);
        test_rule_w_input("parse a metathesis rule with a tl class", U"{ab, cd, ef}/+/_", r, metathesis);
        test_rule_w_input("parse a metathesis rule with more than one plus", U"ab/++++/_", r, metathesis);
    }

    /// <class-def>  ::= TEXT [ "=" ] <simple-el>
    test_stage = "Parser: <class-def>";
    {
        parser p;
        test(p, "parse a simple class definition", U"a = {b, c}", DO(return p.parse_class_def()));
        test(p, "define a class with the correct name", U"a = {b, c}", DO(return p.classes.contains(U"a")));
        test(p, "define a class with the correct contents", U"a = {b, c}",
            DO(return p.classes[U"a"].els == el_vec(U"b", U"c")));

        test(p, "report an error when attempting to redefined a class",
            U"a = {b, c}", DO(return p.parse_class_def()), error);
    }

    {
        parser p;
        test(p, "parse a complex class definition", U"q = {b, {d, f, g}, c}", DO(return p.parse_class_def()));
        test(p, "define a class with name", U"q = {b, {d, f, g}, c}", DO(return p.classes.contains(U"q")));
        test(p, "define a class with the correct contents", U"q = {b, {d, f, g}, c}",
            DO(return p.classes[U"q"].els == el_vec(U"b", U"d", U"f", U"g", U"c")));
    }

    test("not define a class if it fails to parse", U"a = {b, c",
        DO(ASSERT(!p.parse_class_def()); return !p.classes.contains(U"a")));

    test("report an error if class definition has no name", U"{b, {d, f, g}, c}",
        DO(return p.parse_class_def()), error);
    test("report an error if class definition has no name", U" = {b, {d, f, g}, c}",
        DO(return p.parse_class_def()), error);

    /// <input>      ::= <input-els>  { "," <input-els> }
    test_stage = "Parser: <input>";
    test("parse simple input", U"a", DO(return p.parse_input().success));
    test("parse input consisting of a list", U"a, b, c", DO(return p.parse_input().success));
    test("parse input consisting of a class", U"{a, b, c}", DO(return p.parse_input().success));

    test("report an error if the input is empty", U"", DO(return p.parse_input().success), error);
    test("report an error if the input contains percentages", U"20%{a, b, c}",
        DO(return p.parse_input().success), error);

    /// <input-els>  ::= { <input-el> }-
    test_stage = "Parser: <input-els>";
    test("parse input elements (one element)", U"a", DO(return p.parse_input_elements().success));
    test("parse input elements (text)", U"abcd", DO(return p.parse_input_elements().success));
    test("parse input elements (classes)", U"{a}{b}{c}{d}", DO(return p.parse_input_elements().success));
    test("parse input elements (mixed)", U"{a}b{c}d", DO(return p.parse_input_elements().success));

    /// <input-el>   ::= <simple-el> | "*"
    test_stage = "Parser: <input-el>";
    test("parse a simple input element", U"abc", DO(return p.parse_input_element().success));
    test("parse \"*\" as an input element", U"*", DO(return p.parse_input_element().success));

    test("report an error if the input contains percentages", U"20%a", DO(return p.parse_input_element().success), error);
    test("report an error if the input contains optionals", U"(a)", DO(return p.parse_input_element().success), error);
    test("report an error if the input contains only a separator", U"/", DO(return p.parse_input_element().success), error);

    /// <output>     ::= <output-els> { "," <output-els> }
    test_stage = "Parser: <output>";
    test("parse simple output", U"a", DO(return p.parse_output().success));
    test("parse output consisting of a list", U"a, b, c", DO(return p.parse_output().success));
    test("parse output consisting of a class", U"{a, b, c}", DO(return p.parse_output().success));
    test("parse output containing percentages", U"%{a, 10%b, c}, a, {r}", DO(return p.parse_output().success));

    test("report an error if the output is empty", U"", DO(return p.parse_output_element().success), error);

    /// <output-els> ::= { <output-el> }-
    test_stage = "Parser: <output-els>";
    test("parse output elements (one element)", U"a", DO(return p.parse_output_elements().success));
    test("parse output elements (text)", U"abcd", DO(return p.parse_output_elements().success));
    test("parse output elements (classes)", U"{a}{b}{c}{d}", DO(return p.parse_output_elements().success));
    test("parse output elements (mixed)", U"{a}b{c}d", DO(return p.parse_output_elements().success));

    /// <output-el>  ::= <percent-alternatives> | <simple-el>
    test_stage = "Parser: <output-el>";
    test("parse a simple output element", U"abc", DO(return p.parse_output_element().success));
    test("parse an output element containing percentage alternatives", U"20%{a}", DO(return p.parse_output_element().success));

    test("report an error if the output contains \"*\"", U"*", DO(return p.parse_output_element().success), error);
    test("report an error if the output contains naked percentages", U"20%r", DO(return p.parse_output_element().success), error);
    test("report an error if the output contains optionals", U"(a)", DO(return p.parse_output_element().success), error);
    test("report an error if the output contains only a separator", U"/", DO(return p.parse_output_element().success), error);

    ///
    /// <context>    ::= SEPARATOR <boundaries> [ <ctx-els> ] { USCORE }- [ <ctx-els> ] <boundaries> EOL
    /// <ctx-els>    ::= [ "~" ] <optional-els>
    /// <boundaries> ::= { "#" | "$" }
    ///
    /// <optional-els> ::= { <optional-el> }-
    /// <optional-el>  ::= <simple-el> | "(" <optional-els> ")"
    ///
    /// <percent-alternatives> ::= PERCENTAGE <percent-class>
    /// <percent-class>        ::= "{" <percent-list> "}"
    /// <percent-list>         ::= <percent-els> { "," <percent-els> }
    /// <percent-els>          ::= [ PERCENTAGE ] { <percent-el> }-
    /// <percent-el>           ::= ( TEXT | <percent-class> )
    ///
    /// <simple-el>            ::= TEXT | <simple-el-class>

    /// <simple-el-class>      ::= <simple-el-class-lit> { <set-op> <simple-el-subtrahend> }
    /// Other than the name, there's nothing simple about this rule.
    /// Due to the fact that this is where the set operations happen, this
    /// rule is probly more complex to test any other rule of this grammar.
    test_stage = "Parser: <simple-el-class>";
    test("parse a class literal as a class", U"{a, b, c}", DO(return p.parse_simple_element_class().success));
    test("parse a class name as a class", U"x foo", DO(p.classes[U"foo"] = {}; p.next_token(); return p.parse_simple_element_class().success));

    /// Catenation
    {
        static constexpr auto f = &parser::parse_simple_element_class;
        if (auto [el, s] = test_element("parse the catenation of two classes", U"{a, b, c}+{d, e, f}", f); s)
            test_classes_eq("evaluate the catenation of two classes", el.els, el_vec(U"ad", U"be", U"cf"));

        if (auto [el, s] = test_element("parse the catenation of two with different element counts", U"{a, b}+{d, e, f}", f); s)
            test_classes_eq("evaluate the catenation of two classes with different element counts", el.els, el_vec(U"ad", U"be", U"f"));

        if (auto [el, s] = test_element("parse the catenation of two with different element counts", U"{a, b, c}+{d, e}", f); s)
            test_classes_eq("evaluate the catenation of two classes with different element counts", el.els, el_vec(U"ad", U"be", U"c"));

        if (auto [el, s] = test_element("parse the catenation of a class and text", U"{a, b, c}+d", f); s)
            test_classes_eq("evaluate the catenation of a class and text", el.els, el_vec(U"ad", U"b", U"c"));

        if (auto [el, s] = test_element("parse the catenation of a single-element class and text", U"{d}+{a, b, c}", f); s)
            test_classes_eq("evaluate the catenation of a single-element class and text", el.els, el_vec(U"da", U"b", U"c"));

        if (auto [el, s] = test_element("parse the catenation of two single-element classes", U"{a}+{c}", f); s)
            test_classes_eq("evaluate the catenation of two single-element classes", el.els, el_vec(U"ac"));

        test("report an error when the lhs of a catenation is text", U"a+{b, c, d}", DO(return p.parse_simple_element_class().success), error);
    }

    /// Difference
    {
        static constexpr auto f = &parser::parse_simple_element_class;
        if (auto [el, s] = test_element("parse the difference of two classes", U"{a, b}~{a, d}", f); s)
            test_classes_eq("evaluate the difference of two classes", el.els, el_vec(U"b"));

        if (auto [el, s] = test_element("parse the difference of a class and text", U"{a, b}~a", f); s)
            test_classes_eq("evaluate the difference of a class and text", el.els, el_vec(U"b"));

        if (auto [el, s] = test_element("warn when a difference operation has no effect", U"{a, b, c}~{d, e}", f, warning); s)
            test_classes_eq("evaluate a difference operation with no effect", el.els, el_vec(U"a", U"b", U"c"));

        if (auto [el, s] = test_element("warn when a difference operation has no effect", U"{a, b, c}~d", f, warning); s)
            test_classes_eq("evaluate a difference operation with no effect", el.els, el_vec(U"a", U"b", U"c"));

        if (auto [el, s] = test_element("warn when a difference results in an empty class", U"{a, b, c}~{a, b, c}", f, warning); s)
            rawtest("evaluate a difference operation that results in an empty class", "(see below)", [el = &el] { return el->empty(); });
    }

    /// Intersection
    {
        static constexpr auto f = &parser::parse_simple_element_class;
        if (auto [el, s] = test_element("parse the intersection of two classes", U"{a, b}&{a, d}", f); s)
            test_classes_eq("evaluate the intersection of two classes", el.els, el_vec(U"a"));

        if (auto [el, s] = test_element("parse the intersection of a class and a single-element class", U"{b, a, d}&{b}", f); s)
            test_classes_eq("evaluate the intersection of a class and a single-element class", el.els, el_vec(U"b"));

        if (auto [el, s] = test_element("parse the intersection of a class and text", U"{a, b}&a", f); s)
            test_classes_eq("evaluate the intersection of a class and text", el.els, el_vec(U"a"));

        if (auto [el, s] = test_element("warn when parse the intersection of a single-element class and a class that has no effect", U"{b}&{b, a, d}", f, warning); s)
            test_classes_eq("evaluate the intersection of a single-element class and a class", el.els, el_vec(U"b"));

        if (auto [el, s] = test_element("warn when a intersection operation has no effect", U"{a, b, c}&{a, b, c}", f, warning); s)
            test_classes_eq("evaluate a intersection operation with no effect", el.els, el_vec(U"a", U"b", U"c"));

        if (auto [el, s] = test_element("warn when a intersection operation has no effect", U"{d}&d", f, warning); s)
            test_classes_eq("evaluate a intersection operation with no effect", el.els, el_vec(U"d"));

        if (auto [el, s] = test_element("warn when a intersection results in an empty class", U"{a, b, c}&{e, d, f}", f, warning); s)
            rawtest("evaluate a intersection operation that results in an empty class", "(see below)", [el = &el] { return el->empty(); });
    }

    /// Union
    {
        static constexpr auto f = &parser::parse_simple_element_class;
        if (auto [el, s] = test_element("parse the union of two disjoint classes", U"{a, b}|{c, d}", f); s)
            test_classes_eq("evaluate the union of two disjoint classes", el.els, el_vec(U"a", U"b", U"c", U"d"));

        if (auto [el, s] = test_element("parse the union of two classes", U"{a, b}|{a, c}", f); s)
            test_classes_eq("evaluate the union of two classes", el.els, el_vec(U"a", U"b", U"c"));

        if (auto [el, s] = test_element("parse the union of a single-element class and a class (disjoint)", U"{c}|{b, a, d}", f); s)
            test_classes_eq("evaluate the union of a single-element class and a class (disjoint)", el.els, el_vec(U"c", U"b", U"a", U"d"));

        if (auto [el, s] = test_element("parse the union of a class and a single-element class (disjoint)", U"{b, a, d}|{c}", f); s)
            test_classes_eq("evaluate the union of a class and a single-element class (disjoint)", el.els, el_vec(U"b", U"a", U"d", U"c"));

        if (auto [el, s] = test_element("parse the union of a single-element class and text (disjoint)", U"{a}|c", f); s)
            test_classes_eq("evaluate the union of a single-element classe and text (disjoint)", el.els, el_vec(U"a", U"c"));

        if (auto [el, s] = test_element("parse the union of a class and text (disjoint)", U"{a, b}|c", f); s)
            test_classes_eq("evaluate the union of a class and text (disjoint)", el.els, el_vec(U"a", U"b", U"c"));

        if (auto [el, s] = test_element("warn when lhs in union already contain rhs", U"{a, b}|a", f, warning); s)
            test_classes_eq("evaluate the union of a class and text", el.els, el_vec(U"a", U"b"));

        if (auto [el, s] = test_element("warn when lhs in union already contains rhs", U"{b, a, d}|{b}", f, warning); s)
            test_classes_eq("evaluate the union of a class and a single-element class", el.els, el_vec(U"b", U"a", U"d"));

        if (auto [el, s] = test_element("warn when the union of a single-element class and a class has no effect", U"{b}|{b, a, d}", f, warning); s)
            test_classes_eq("evaluate the union of a single-element class and a class", el.els, el_vec(U"b", U"a", U"d"));

        if (auto [el, s] = test_element("warn when a union operation has no effect", U"{a, b, c}|{a, b, c}", f, warning); s)
            test_classes_eq("evaluate a union operation with no effect", el.els, el_vec(U"a", U"b", U"c"));

        if (auto [el, s] = test_element("warn when a union operation has no effect", U"{d}|d", f, warning); s)
            test_classes_eq("evaluate a union operation with no effect", el.els, el_vec(U"d"));
    }

    /// <simple-el-subtrahend> ::= <simple-el-class-lit> | TEXT
    /// <simple-el-class-lit>  ::= CLASS-NAME | "{" <simple-el-list> "}"
    /// <simple-el-list>       ::= <simple-els> { "," <simple-els> }
    /// <simple-els>           ::= { <simple-el> }-

    /// <rule> ::= <substitution-rule>
    ///          | <epenthesis-rule>
    ///          | <deletion-rule>
    ///          | <metathesis-rule>
    ///          | <reduplication-rule>
    test_stage = "Parser: <rule>";
    test("recognise a substitution rule", U"a/b/_", DO(return p.parse_rule() && p.rules[0].type == substitution));
    test("recognise an epenthesis rule", U"/b/_b", DO(return p.parse_rule() && p.rules[0].type == epenthesis));
    test("recognise a deletion rule", U"a//_", DO(return p.parse_rule() && p.rules[0].type == deletion));
    test("recognise a metathesis rule", U"ab/&/_", DO(return p.parse_rule() && p.rules[0].type == metathesis));
    test("recognise a reduplication rule", U"ca/+/_", DO(return p.parse_rule() && p.rules[0].type == reduplication));

    ///
    /// Miscellaneous tests
    ///
    agma_tests();

    fmt::print(stderr, "Test run:     {}\n"
                       "     success: {}\n"
                       "     failed:  {}\n"
                       "     errored: {}\n",
        run, run - failed - errored, failed, errored);
    exit((bool) (errored + failed));
}
