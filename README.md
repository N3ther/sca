# SCA++
[SCA++](https://www.nguh.org/tools/sca) is a **S**ound **C**hange **A**pplier 
powered by `libsca++`, a library for parsing, compiling, and applying sound 
changes written in C++ 20.

The documentation on how to use SCA++ is available either from the `Help` menu
in the application itself, or alternatively [here](doc/syntax.md).

## How to Get SCA++
There are multiple ways to get SCA++: you can either access it online on
nguh.org, or download an app that you can use locally on your PC.

### On nguh.org
You can access SCA++ online by navigating to <https://www.nguh.org/tools/sca>.

### Linux
You can download linux builds from nguh.org here: 
- [sca++-linux-x64.tar.gz](https://www.nguh.org/apps/sca/sca++-linux-x64.tar.gz)

### Other Platforms
If you're on a platform that is not listed above, you will either have to use
the version on [nguh.org](https://www.nguh.org/tools/sca) or build it from source.

## Motivation
This project exists because we were displeased with the other sound change
appliers out there: they were all either too simplistic, too clunky, or much
too verbose and complicated.

SCA++ aims to provide an easy-to-use, but powerful, syntax for defining sound changes.
The syntax, roughly based on the popular SCA², was devised in great part by [Agma Schwa](https://www.nguh.org),
who, as a linguist and someone with more than a decade's worth of experience in conlanging and
years-long and ongoing involvement in the conlanging community, has helped not only 
identify the problems present in but also improve upon the syntaxes of most of the other 
sound change appliers out there.

You can still make use of the old SCA² syntax and, at some point in the future,
SCA++ will be able to automatically update any old SCA² rules that you might have 
lying around to our new syntax. 

We are also in frequent contact with many other personalities in the conlanging
community who continuously help improve SCA++ by informing us about their use of
and problems with this and other sound change appliers.

## Features
Compared to SCA², SCA++'s use of `,` as a delimiter allows you to make use
of digraphs, diacritics, and other multi-character sequences without any problems. 
This, in our opinion, is one of the most important improvements over SCA², 
and most modern sound change appliers support this syntax as well.

SCA++ understands Unicode and makes use of it to ensure that diacritics
and other modifier letters, such as `ʰ`, remain attached to the letter that
precedes them, even when performing metathesis. Tie bars `t͡ʃ` are also supported.

Furthermore, SCA++ also attempts to respond with helpful error messages and warnings 
should you make a mistake somewhere. These error messages will also tell you 
where exactly the error occurred. Of course, the error messages aren't perfect; should
you get a message that doesn't make sense to you, don't hesitate to [contact us](#contributing-and-bug-reports) 
so that we can improve them.

## Building from Source
Building SCA++ from source works slightly differently depending on whether you want to build
the native version or the wasm version. In both cases, however, the first thing you need to do
is clone this repository (make sure to use `--recursive` because we have submodules):
```bash
$ git clone --recursive https://gitlab.com/agma-schwa-public/sca
```
SCA++ consists of two parts. First, there is `libsca++`, the SCA library that
parses, compiles, and then applies the sound changes; and secondly, there is
an electron-based ui for inputting sound changes.

In any case, you'll need to have
- a C++ compiler;
- a posix-compliant shell;
- cmake, make, ninja;
- sed.

### Native Version
In the project root, run:

```bash
$ ./build.sh
$ cd ./frontend 
$ ./build.sh
```
The `frontend/package` directory will contain the native SCA++ build. The
main executable is named `sca++`.

### Web Assembly Version
By following these instructions, you can obtain *a* WASM build of SCA++. Note
that what you'll get will *not* be exactly the same as the version of SCA++ that
you can find on nguh.org; the latter also includes nguh.org-specific stylesheets,
and the html and css for it are generated from the native project files, with 
some programmatic modifications so as to make it fit in with the rest of nguh.org.

To build SCA++ for WASM, you first need to install the latest version of
[Emscripten](https://emscripten.org/docs/getting_started/downloads.html).

Then, in the project root, run:
```bash
$ ./build.sh wasm
```

The `frontend/wasm-build` directory will contain `libsca++` for WASM.

## Contributing and Bug Reports
If you want to report a bug, contribute to, fork, or otherwise make use of 
the source code of this project, here's a few things you might find useful.

If you have any questions, feel free to contact us on the [Agma Schwa 
Discord server](https://discord.gg/X56KdfQA7N). Just use the `/ticket` 
command or, if no-one gets back to you, @ the ‘Mod’ role, and someone should 
show up and help you out.

### Reporting a Bug
Please file an issue and include the exact combination (or preferably, a 
minimal reproducible example) of classes, rules, words, (and rewrite rules,
if you're using the SCA² mode) that caused the bug. You can make use of SCA++'s
`File -> Save As` menu option to export your session to a `.sca.json` file.

If the bug is accompanied by warnings and/or errors, please include a 
screenshot of them in your bug report.

### Directories
The following is a brief description of the directory structure of this project:
- `3rdparty`: third-party dependencies for `libsca++`.
- `doc`: SCA++ syntax documentation and BNFs.
- `frontend`: the SCA++ UI for Electron and Web Assembly.
  - `assets`: icons etc.
  - `css`, `js`: client-side `css` and `js` for SCA++.
  - `src`: Electron driver code and `html` files.
    - `electron`: Electron-specific frontend and backend code.
    - `web`: WASM-specific frontend and backend code.
- `lib`: this directory contains `libsca++` as well as its dependencies.
- `test`: unit tests and test programs for `libsca++`.
