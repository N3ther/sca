const SCA_PLUS_PLUS_MODE = 0
const SCA_2_MODE = 1

let code_mirrors = ['classes', 'rules', 'words', 'output']
let classes, rules, words, output
let root = document.querySelector(':root')
let sca_mode = SCA_PLUS_PLUS_MODE
let hightlight_classes = new Set
let settings
let title_bar_height

const __favicon_path = window.electron ? __dirname + '/../assets/icon.svg' : '/apps/sca/icon.svg'
const __version_self = '0.9 (beta)'
const __version_deps = window.electron
    ? `Chrome: ${window.__versions[0]}<br>Node JS: ${window.__versions[1]}<br>Electron: ${window.__versions[2]}`
    : ''

console.__log = console.log
console.__error = console.error

if (!window.electron) {
    window.read_settings_file = () => { return localStorage.getItem('settings') ?? '{}' }
    window.write_to_settings_file = (str) => { return localStorage.setItem('settings', str) }
}

/// Settings and customisation
const colour_pickers = [
    {
        title: 'Syntax Highlighting',
        data: [
            ['--cm-comment-colour', 'Comment'],
            ['--cm-text-colour', 'Text'],
            ['--cm-error-colour', 'Error'],
            ['--cm-class-name-colour', 'Class'],
            ['--cm-separator-colour', 'Separator'],
            ['--cm-brace-colour', 'Braces'],
            ['--cm-brack-colour', 'Brackets'],
            ['--cm-paren-colour', 'Parentheses'],
            ['--cm-hash-colour', 'Hash Sign'],
            ['--cm-dollar-colour', 'Dollar Sign'],
            ['--cm-ampersand-colour', 'Ampersand'],
            ['--cm-plus-colour', 'Plus'],
            ['--cm-uscore-colour', 'Underscore'],
            ['--cm-assign-colour', 'Equals Sign'],
            ['--cm-tilde-colour', 'Tilde'],
            ['--cm-comma-colour', 'Comma'],
            ['--cm-wildcard-colour', 'Wildcard'],
            ['--cm-star-colour', 'Star'],
            ['--cm-vbar-colour', 'Vertical Bar'],
            ['--cm-percentage-colour', 'Percentage'],
        ]
    },
    {
        title: 'SCA 2 Syntax Highlighting',
        data: [
            ['--cm-z_dbl_backslash-colour', 'Double Backslash'],
            ['--cm-z_superscript_2-colour', 'Superscript ²']
        ]
    },
    {
        title: 'Window',
        data: [
            ['--fg-colour', 'Foreground'],
            ['--fg-legend', 'Legends'],
            ['--bg-colour', 'Background'],
            ['--bg-accent', 'Accents'],
            ['--bg-border', 'Border'],
            ['--title-bar-colour', 'Title Bar'],
            ['--buttons-colour', 'Buttons']
        ]
    }
]

try {
    settings = JSON.parse(window.read_settings_file())
} catch (e) {
    console.error(e)
    settings = {}
}

function load_settings() {
    if ('css_properties' in settings) {
        for (const [name, value] of settings.css_properties)
            root.style.setProperty(name, value)
    }

    if ('classes' in settings) classes.setValue(settings.classes)
    if ('rules' in settings) rules.setValue(settings.rules)
    if ('input' in settings) words.setValue(settings.input)
    if ('output' in settings) output.setValue(settings.output)
}

function save_settings() {
    let cs = getComputedStyle(root)

    settings.css_properties = []
    for (const {data} of colour_pickers)
        for (const [property,] of data)
            settings.css_properties.push([property, cs.getPropertyValue(property).trim()])

    settings.classes = classes.getValue()
    settings.rules = rules.getValue()
    settings.input = words.getValue()
    settings.output = output.getValue()

    window.write_to_settings_file(JSON.stringify(settings))
}

window.addEventListener('beforeunload', (e) => {
    save_settings()
})

function new_session() {
    for (let cm of [classes, rules, words, output]) cm.setValue('')
    delete settings.savefile
}

///
/// Miscellaneous
///

function clamp(val, lo, hi) {
    if (lo > hi) return lo
    return val < lo ? lo : val > hi ? hi : val
}

/// See https://stackoverflow.com/questions/1740700/how-to-get-hex-color-value-rather-than-rgb-value
const rgba2hex = (rgba) => `#${rgba.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+\.{0,1}\d*))?\)$/).slice(1).map((n, i) => (i === 3 ? Math.round(parseFloat(n) * 255) : parseFloat(n)).toString(16).padStart(2, '0').replace('NaN', '')).join('')}`

function colour_as_hex(c) {
    return c.startsWith('#') ? c : rgba2hex(c)
}

function results_as_csv() {
    const input = words.getValue().split('\n').filter(s => s.length)
    const out = output.getValue().split('\n').filter(s => s.length)
    let str = ''

    let i = 0
    for (; i < Math.min(input.length, out.length); i++) str += `${input[i]},${out[i]}\n`
    if (input.length < out.length) for (; i < out.length; i++) str += `,${out[i]}\n`
    else if (input.length > out.length) for (; i < input.length; i++) str += `${input[i]},\n`

    return str
}

function make_fieldset(title) {
    const f = document.createElement('fieldset')
    f.className = 'fieldset-horizontal'

    const legend = document.createElement('legend')
    legend.innerText = title
    f.appendChild(legend)

    return f
}

function translate_term_colours(str) {
    let ret = str
    ret = ret.replaceAll(/\033\[31m(.*?)\033\[0?m/g, '<span class="term-red">$1</span>')
    ret = ret.replaceAll(/\033\[1;31m(.*?)\033\[0?m/g, '<span class="term-red term-bold">$1</span>')
    ret = ret.replaceAll(/\033\[32m(.*?)\033\[0?m/g, '<span class="term-green">$1</span>')
    ret = ret.replaceAll(/\033\[1;32m(.*?)\033\[0?m/g, '<span class="term-green term-bold">$1</span>')
    ret = ret.replaceAll(/\033\[33m(.*?)\033\[0?m/g, '<span class="term-yellow">$1</span>')
    ret = ret.replaceAll(/\033\[1;33m(.*?)\033\[0?m/g, '<span class="term-yellow term-bold">$1</span>')
    ret = ret.replaceAll(/\033\[34m(.*?)\033\[0?m/g, '<span class="term-blue">$1</span>')
    ret = ret.replaceAll(/\033\[1;34m(.*?)\033\[0?m/g, '<span class="term-blue term-bold">$1</span>')
    ret = ret.replaceAll(/\033\[35m(.*?)\033\[0?m/g, '<span class="term-magenta">$1</span>')
    ret = ret.replaceAll(/\033\[1;35m(.*?)\033\[0?m/g, '<span class="term-magenta term-bold">$1</span>')
    ret = ret.replaceAll(/\033\[1;38m(.*?)\033\[0?m/g, '<span class="term-white term-bold">$1</span>')
    return ret
}

function show_diagnostics(messages) {
    let errmsg = ''
    const errs = messages.split('\f\f')
    for (const err of errs) {
        let lines = err.split('\n')
        let line1 = lines[0].split(']')
        let line_number = line1.shift()
        line1.join(']')

        let diag = lines.length >= 3 ? `<span class="term-bold">${line_number}]</span>${line1}<pre>${lines[1]}\n${lines[2]}</pre>` : err
        errmsg += `<div class="sca-diagnostic-${lines.length >= 3 ? lines[0].match(/(Error|Warning|Note):/g)[0].slice(0, -1) : ''}">${translate_term_colours(diag)}</div>`
    }

    Dialog.error(errmsg, {title: 'Errors & Warnings', classes: ['sca-diagnostic-dialog']})
}

function show_exception(e) {
    let message = translate_term_colours('\033[1;31mException raised:\033[m '
        + e.message.replaceAll('\n', '<br>')
        + '<br>\033[1;31mTrace:\033[m<br>');
    if ('stack' in e) {
        let stack = e.stack
            .split('\n')
            .map(l => {
                if (l.includes('libsca++.wasm'))
                    return l.replace(/(.*?)\(?http:\/\/localhost\/apps\/sca\/libsca\+\+\.wasm:wasm-function/, '\033[32m$1\033[m[libsca++]')
                else if (l.includes('libsca++.js')) return l.replace(/(.*?)\(?http:\/\/localhost\/apps\/sca\/libsca\+\+\.js\?v=\d+/, '\033[33m$1\033[m[libsca++-js]')
                else return l;
            })
            .join('<br>')
        message += translate_term_colours(stack)
    }
    Dialog.error(`<div class="sca-diagnostic-Error"><pre>${message}</pre></div>`,
        {title: 'Errors & Warnings', classes: ['sca-diagnostic-dialog']})
}

///
/// SCA proper
///
function apply_sound_changes() {
    rehighlight_all()

    /// Try to perform the sound change application.
    /// If everything went well, display the output.
    ///
    /// This can fail in a multitude of different ways.
    /// SCA++ Diagnostics are terminated by \f\f.
    try {
        let res = window.sca.parse_and_apply(classes.getValue(), rules.getValue(), words.getValue(), sca_mode);
        if (!window.electron) res = res.split('\v\v')
        let [result, diags] = res
        output.setValue(result)
        if (diags.length) show_diagnostics(diags)
    } catch (e) {
        /// If this is some random error, just show the error dialog.
        /// Otherwise, format the error messages.
        console.log(e);

        /// Emscripten gives us a pointer to native exceptions as a number.
        /// Call into C++ to cast it to an exception and retrieve the message.
        if (typeof e === 'number') e = {message: window.electron ? String(e) : Module.exception_message(e)}

        if (!('message' in e)) {
            if (!e) e = {}
            else if (typeof e === 'string') e = {message: e};
            show_exception(e)
        } else if (!e.message.includes('\f\f')) show_exception(e)
        else show_diagnostics(e.message)
    }
}

let current_mode_indicator = document.getElementById('current-mode-indicator')

function set_mode(mode) {
    sca_mode = mode
    switch (sca_mode) {
        case SCA_PLUS_PLUS_MODE:
            current_mode_indicator.innerText = 'SCA++ Mode'
            break
        case SCA_2_MODE:
            current_mode_indicator.innerText = 'SCA 2 Mode'
            break
        default:
            current_mode_indicator.innerText = '(error)'
    }
}

set_mode(SCA_PLUS_PLUS_MODE)
///
/// Dialogs
///
class Dialog {
    handle
    __atopen = []

    /// This is so we can make sure that the dialogs don't end up
    /// out of bounds when we resize the window
    static open_dialogs = new Set
    static close_button_template = document.getElementById('close-button-template').content

    constructor(handle) {
        this.handle = handle
        this.ephemeral = true

        /// Make the dialog draggable.
        Draggable(handle, handle.getElementsByTagName('div')[0])
    }

    at_open(fun) {
        this.__atopen.push(fun)
    }

    close() {
        this.handle.close()
        this.handle.style.display = 'none';
        Dialog.open_dialogs.delete(this)
    }

    open() {
        this.handle.style.display = 'flex';
        this.handle.showModal()

        /// Centre it.
        this.handle.style.top = clamp_yoffs(innerHeight / 2 - this.handle.scrollHeight / 2, this.handle) + 'px'
        this.handle.style.left = clamp_xoffs(innerWidth / 2 - this.handle.scrollWidth / 2, this.handle) + 'px'

        /// Run registered hooks.
        for (let __atopen_fun of this.__atopen) __atopen_fun(this)

        Dialog.open_dialogs.add(this)
    }

    static make(title, content, controls = [], id) {
        /// Create the dialog element.
        const handle = document.createElement('dialog')
        handle.className = 'modal'
        document.body.appendChild(handle)

        /// Add the id if we've specified one.
        if (typeof id === 'string') handle.id = id

        /// Add the title bar.
        const title_bar = document.createElement('div')
        title_bar.className = 'dialog-title'
        handle.appendChild(title_bar)

        /// Title bar text.
        const title_bar_text = document.createElement('div')
        title_bar_text.className = 'dialog-title-content'
        title_bar_text.innerHTML = title
        title_bar.appendChild(title_bar_text)

        /// Close button.
        const close_button = Dialog.close_button_template.cloneNode(true).children[0]
        title_bar.appendChild(close_button)

        /// Content.
        const content_element = document.createElement('div')
        content_element.className = 'dialog-content'
        handle.appendChild(content_element)

        if (typeof content === 'string') content_element.innerHTML = content
        else if (typeof content === 'object' && content) content_element.appendChild(content)

        /// Add the controls, if any.
        if (typeof controls === 'string') controls = [controls]
        if (controls.length) {
            /// Controls Wrapper.
            const controls_element = document.createElement('div')
            controls_element.className = 'dialog-controls'
            handle.appendChild(controls_element)

            /// Create the controls.
            for (const control_name of controls) {
                const control = document.createElement('button')
                control.innerText = control_name
                controls_element.appendChild(control)
            }
        }

        /// Create the dialog.
        let dialog = new Dialog(handle)
        close_button.onclick = () => {
            dialog.close()
            if (dialog.ephemeral) dialog.handle.remove()
        }
        return dialog
    }

    static open_confirm_dialog(title, text) {
        /// Create a new dialog.
        const dialog = Dialog.make(title, text, ['Yes', 'Cancel'])

        /// We'll override the 'yes' and 'cancel' buttons.
        const [yes, cancel] = dialog.handle.getElementsByTagName('button')
        const close = dialog.handle.getElementsByTagName('svg')[0]


        /// Open it.
        document.body.appendChild(dialog.handle)
        dialog.open()

        return new Promise((resolve) => {
            yes.onclick = () => {
                dialog.close()
                dialog.handle.remove()
                resolve(true)
            }

            close.onclick = cancel.onclick = () => {
                dialog.close()
                dialog.handle.remove()
                resolve(false)
            }
        })
    }

    static error(text, options = {}) {
        /// Create a new dialog.
        const dialog = Dialog.make('Error', null)
        dialog.handle.children[1].classList.add('error-dialog-content')

        /// Apply settings.
        if ('title' in options) dialog.handle.getElementsByTagName('div')[0].children[0].innerHTML = options.title
        if ('classes' in options) for (let c of options.classes) dialog.handle.classList.add(c)

        /// Set the text.
        dialog.handle.children[1].innerHTML = text

        /// Open it.
        document.body.appendChild(dialog.handle)
        dialog.open()
    }

    static clamp_open_dialogs_x(window_x) {
        for (let dialog of Dialog.open_dialogs)
            dialog.handle.style.left = clamp_xoffs(dialog.handle.offsetLeft, dialog.handle, window_x) + 'px'
    }

    static clamp_open_dialogs_y(window_y) {
        for (let dialog of Dialog.open_dialogs)
            dialog.handle.style.top = clamp_yoffs(dialog.handle.offsetTop, dialog.handle, window_y) + 'px'
    }
}

/// Clamp an x or y offset (CSS left/top) within the window.
function clamp_xoffs(xoffs, el, window_x = innerWidth) {
    const border_width = el.offsetWidth - el.clientWidth
    return clamp(xoffs, 0, window_x - el.scrollWidth - border_width)
}

function clamp_yoffs(yoffs, el, window_y = innerHeight) {
    const border_height = el.offsetHeight - el.clientHeight
    return clamp(yoffs, title_bar_height, window_y - el.scrollHeight - border_height)
}

/// Make `container` draggable when clicking on `header`.
function Draggable(container, header) {
    /// Move `container` when we click and drag `header`
    header.addEventListener('mousedown', drag)

    /// This does most of the actual work.
    function drag(e) {
        e.preventDefault()

        /// Position where we start dragging.
        let x_drag_start = parseInt(e.clientX)
        let y_drag_start = parseInt(e.clientY)

        /// Move the element when the mouse moves. Stop dragging when we
        /// release the element.
        document.addEventListener('mouseup', stop_dragging)
        document.addEventListener('mousemove', do_drag)

        /// Move the element.
        function do_drag(e) {
            e.preventDefault()

            /// The x/y distance we need to move.
            const xdelta = x_drag_start - parseInt(e.clientX)
            const ydelta = y_drag_start - parseInt(e.clientY)

            /// The new top/left position
            /// Make sure we don't drag the element outside the window
            let new_x_offs = clamp_xoffs(container.offsetLeft - xdelta, container)
            let new_y_offs = clamp_yoffs(container.offsetTop - ydelta, container);

            /// The current position becomes the new start position.
            x_drag_start = parseInt(e.clientX)
            y_drag_start = parseInt(e.clientY)

            /// Move the element.
            container.style.top = new_y_offs + 'px'
            container.style.left = new_x_offs + 'px'
        }

        /// Stop dragging when the element is released.
        function stop_dragging() {
            document.removeEventListener('mouseup', stop_dragging)
            document.removeEventListener('mousemove', do_drag)
        }
    }

}

/// Preferences dialog.
const pref_dialog_content = document.getElementById('preferences-dialog-content-template').content.cloneNode(true).children[0]
const pref_dialog = Dialog.make('Preferences', pref_dialog_content)
pref_dialog.ephemeral = false

/// Customise dialog.
const customise_dialog = Dialog.make('Customise SCA++', null, ['Reset'])
const customise_dialog_content = customise_dialog.handle.children[1]
customise_dialog.ephemeral = false
customise_dialog.handle.getElementsByTagName('button')[0].onclick = reset_customisations

/// Version dialog.
const version_dialog = Dialog.make("Version", `<img src="${__favicon_path}" id="version-icon">
<div><p>SCA++ Version ${__version_self}</p><br><p>${__version_deps}</p></div>`, [], 'version-dialog')
version_dialog.ephemeral = false

/// Create and append colour picker elements for the colours in `property_name_list`.
function create_colour_pickers(title, property_name_list) {
    /// Create a fieldset to wrap the colour pickers.
    let fieldset = document.createElement('fieldset')
    fieldset.className = 'dialog-options'

    /// The legend for the fieldset.
    let legend = document.createElement('legend')
    legend.innerText = title
    fieldset.appendChild(legend)

    /// Create a div to wrap the colour pickers.
    let inputs_wrapper = document.createElement('div')
    inputs_wrapper.className = 'customise-dialog-inputs'
    fieldset.appendChild(inputs_wrapper)

    /// For initialising the input values.
    let cs = getComputedStyle(root)

    /// Append the inputs
    for (const [colour_property_name, label_text] of property_name_list) {
        /// Wrap the label and input in a div to make sure they stay on the same line.
        let div = document.createElement('div')
        inputs_wrapper.appendChild(div)

        /// Colour picker
        let input = document.createElement('input')
        input.type = 'color'
        input.id = `input-customise${colour_property_name}`
        input.oninput = () => root.style.setProperty(colour_property_name, input.value)
        input.value = colour_as_hex(cs.getPropertyValue(colour_property_name).trim())
        div.appendChild(input)

        /// Property name.
        let label = document.createElement('label')
        label.htmlFor = `input-customise${colour_property_name}`
        label.innerText = label_text
        div.appendChild(label)
    }

    /// Append the fieldset.
    customise_dialog_content.appendChild(fieldset)
}

for (const {title, data} of colour_pickers) create_colour_pickers(title, data)

function reset_customisations() {
    Dialog.open_confirm_dialog('Reset Customisations', 'This will discard all customisations.<br><br>Proceed?').then((yes) => {
        if (!yes) return

        /// Reset colours.
        for (const {data} of colour_pickers)
            for (const [property,] of data)
                root.style.removeProperty(property)

        /// Update colour pickers.
        const cs = getComputedStyle(root)
        for (const {data} of colour_pickers)
            for (const [property,] of data)
                document.getElementById(`input-customise${property}`).value = colour_as_hex(cs.getPropertyValue(property).trim())
    })
}

///
/// Menu Bar
///

/// This is similar to nguh.org's MouseoverContext.
const hover_menu = {
    elements: [...document.getElementById('menu-bar-items').children],
    toggled: false,
    __element_on_click_listeners: [],

    __enter_element(el) {
        hover_menu.leave_all()
        let subm = el.getElementsByClassName('menu-bar-submenu')
        if (subm.length) subm[0].style.display = 'flex'
    },

    __leave_element(el) {
        let subm = el.getElementsByClassName('menu-bar-submenu')
        if (subm.length) subm[0].style.display = 'none'
    },

    enter_element() { hover_menu.__enter_element(this) },
    leave_element() { hover_menu.__leave_element(this) },
    leave_all() { hover_menu.elements.forEach(el => hover_menu.__leave_element(el)) },

    toggleoff() {
        hover_menu.elements.forEach(el => el.removeEventListener('mouseenter', hover_menu.enter_element))
        hover_menu.leave_all()
    },

    toggleon(el) {
        hover_menu.elements.forEach(el => el.addEventListener('mouseenter', hover_menu.enter_element))
        hover_menu.__enter_element(el)
    },

    perform_click_on_element(el, e) {
        if (hover_menu.toggled) hover_menu.toggleoff()
        else hover_menu.toggleon(el)
        hover_menu.toggled = !hover_menu.toggled

        if (hover_menu.toggled) window.addEventListener('click', hover_menu.perform_click_on_window)
        else window.removeEventListener('click', hover_menu.perform_click_on_window)
        e.stopPropagation()
    },

    perform_click_on_window() {
        if (hover_menu.toggled) {
            hover_menu.toggleoff(true)
            hover_menu.toggled = false
            hover_menu.entered = false
        }
    },
}

hover_menu.elements.forEach(el => {
    const listener = (e) => hover_menu.perform_click_on_element(el, e)
    hover_menu.__element_on_click_listeners.push(listener)
    el.addEventListener('click', listener)
    hover_menu.leave_all()
})

/// Now that all menus are collapsed, we can compute this.
title_bar_height = window.electron ? document.getElementById('title-bar').scrollHeight : 0

///
/// Code Mirror
///

CodeMirror.defineMode('sca', (conf, mode_conf) => {
    const char_0 = '0'.charCodeAt(0)
    const char_9 = '9'.charCodeAt(0)

    function is_number(c) {
        return c !== null && c >= char_0 && c <= char_9
    }

    function is_special_character(c) {
        switch (c) {
            case '/':
            case '>':
            case '{':
            case '}':
            case '[':
            case ']':
            case '(':
            case ')':
            case '#':
            case '$':
            case '%':
            case '*':
            case '?':
            case '&':
            case '+':
            case '_':
            case '=':
            case '~':
            case ',':
                return true;
            default:
                if (sca_mode === SCA_2_MODE && (c === '-' || c === '.' || c === '…')) return true
                return c === null || c === undefined || c.charCodeAt(0) < 32;
        }
    }

    function isspace(c) {
        switch (c) {
            case ' ':
            case '\t':
            case '\n':
            case '\r':
            case '\f':
            case '\v':
                return true;
            default:
                return false;

        }
    }

    return {
        startState() {
            return {}
        },

        token(stream, state) {
            let tok = stream.next()
            if (isspace(tok)) {
                while (isspace(stream.peek())) stream.next()
                return null
            }
            switch (tok) {
                case null:
                case undefined:
                    return null

                /// Single-char tokens
                /// @formatter:off
                case '/':
                    return 'separator'
                case '>':
                    return 'arrow'
                case '{':
                    return 'lbrace'
                case '}':
                    return 'rbrace'
                case '[':
                    return 'lbrack'
                case ']':
                    return 'rbrack'
                case '(':
                    return 'lparen'
                case ')':
                    return 'rparen'
                case '#':
                    return 'hash'
                case '$':
                    return 'dollar'
                case '&':
                    return 'ampersand'
                case '=':
                    return 'assign'
                case '~':
                    return 'tilde'
                case ',':
                    return 'comma'
                case '?':
                    return 'wildcard'
                case '*':
                    return 'star'
                case '|':
                    return 'vbar'
                case '_':
                    return 'uscore'
                case '+':
                    return 'plus'
                /// @formatter:on

                /// SCA2 tokens
                case '²':
                    if (sca_mode === SCA_2_MODE) return 'z_superscript_2'
                    break
                case '…':
                    if (sca_mode === SCA_2_MODE) return 'z_ellipsis'
                    break
                case '.':
                    if (sca_mode === SCA_2_MODE) {
                        if (stream.peek() === '.') {
                            stream.next();
                            if (stream.peek() === '.') {
                                stream.next()
                                return 'z_ellipsis'
                            }
                        }
                    }
                    break
                case '-':
                    if (sca_mode === SCA_2_MODE) return 'line-z_comment'
                    break
                case '→':
                    if (sca_mode === SCA_2_MODE) return 'separator'
                    break
                case '\\':
                    if (sca_mode === SCA_2_MODE) {
                        if (stream.peek() === '\\') {
                            stream.next()
                            return 'z_dbl_backslash'
                        }
                    }
                    break

                /// Complicated stuff
                case '%': {
                    /// A '%' without a number is equal to 100% so long as the next token is \"{\".
                    if (stream.peek() !== '{') return 'error'
                    return 'percentage'
                }
            }

            let charv = tok.charCodeAt(0)
            if (is_number(charv)) {
                while (is_number(stream.peek())) stream.next()
                if (stream.peek() === '%') {
                    stream.next()
                    return 'percentage'
                }
            }

            while (!is_special_character(stream.peek())) stream.next()
            if (stream.peek() === '=') {
                hightlight_classes.add(stream.current())
                return 'class_name'
            }

            return hightlight_classes.has(stream.current()) ? 'class_name' : 'text'
        }
    }
})

function rehighlight_all() {
    hightlight_classes.clear()
    classes.setValue(classes.getValue())
    rules.setValue(rules.getValue())
}

function create_code_mirror(element, raw = false, config = {}) {
    return CodeMirror((el) => {
        element.parentNode.replaceChild(el, element)
    }, {
        ...config,
        mode: raw ? null : 'sca',
        lineWrapping: true,
        lineNumbers: true,
        spellcheck: false,
        autocorrect: false,
        autocapitalize: false,
    })
}

///
/// Called when DOM is loaded
///
function load() {
    ///
    /// Menu Bar
    ///
    const items = document.getElementById('menu-bar-items')

    /// Position submenus below the menu names.
    for (const child of items.children) {
        let submenu = child.getElementsByClassName('menu-bar-submenu')
        if (submenu.length >= 1) submenu[0].style.left = child.offsetLeft
    }

    ///
    /// Code Mirror
    ///

    let cl_el = document.getElementById('classes-input')
    let rules_el = document.getElementById('rules-input')
    let input_el = document.getElementById('words-input')
    let output_el = document.getElementById('words-output')

    classes = create_code_mirror(cl_el)
    rules = create_code_mirror(rules_el)
    words = create_code_mirror(input_el, true)
    output = create_code_mirror(output_el, true, {readOnly: true})


    /// Check if we need saving.
    let saved = true
    for (let cm of [classes, rules, words, output]) {
        if (cm.getValue().length === 0) {
            saved = false
            break
        }
    }
    menu.saved = saved

    ///
    /// Dialogs
    ///
    if (window.electron) {
        ipcRenderer.on('resized', (e, data) => {
            if (data.newsize[0] < data.oldsize[0]) Dialog.clamp_open_dialogs_x(data.newsize[0])
            if (data.newsize[1] < data.oldsize[1]) Dialog.clamp_open_dialogs_y(data.newsize[1])
        })
    } else {
        let oldsize = [window.innerWidth, window.innerHeight]
        window.addEventListener('resize', () => {
            if (window.innerWidth < oldsize[0]) Dialog.clamp_open_dialogs_x(window.innerWidth)
            if (window.innerHeight < oldsize[1]) Dialog.clamp_open_dialogs_y(window.innerHeight)
            oldsize = [window.innerWidth, window.innerHeight]
        })
    }

    /// Reroute console to terminal if we're running in debug mode.
    if (window.electron && window.__debug_mode) console.log = console.error = __dbg

    /// Settings
    load_settings()
    if (Object.keys(settings).length === 0) {
        if (window.electron) localStorage.clear()
        classes.setValue(`V={a, e, i, o, u}
Long={ā, ē, ī, ō, ū}
C={p, t, c, q, b, d, g, m, n, l, r, h, s}
F={i, e}
B={o, u}
S={p, t, c}
Z={b, d, g}`)

        rules.setValue(`{s,m}//_#
i/j/_V
Long/V/_
e//{V}r_#
v//V_V
u/o/_#
gn/nh/_
S/Z/V_V
c/i/F_t
c/u/B_t
p//V_t
ii/i/_
e//C_r{V}
lj/lh/_`)

        words.setValue(`lector
doctor
focus
jocus
districtus
cīvitatem
adoptare
opera
secundus
fīliam
pōntem`)
    }
}

window.addEventListener('DOMContentLoaded', load)
