{
    "variables": {
        "openssl_fips" : "0"
    },
	"targets": [
		{
			"target_name": "sca",
			"cflags_cc": [
				"-std=gnu++2b",
				"-fexceptions",
				"-Wall",
				"-Wextra",
				"-Wundef",
				"-Werror=return-type",
				"-Wconversion",
				"-Wno-unused-function",
				"-Wno-unused-parameter",
				"-Wno-unused-variable",
				"-Wno-empty-body",
				"-Wno-nonnull",
				"-fopenmp"
			],
			"libraries": [ "../../out/libsca++.a", "../../out/3rdparty/fmt/libfmt.a", "../deps/libicuuc.so", "-lgomp" ],
			"sources": [
				"src/electron/binding.cc"
			]
		}
	]
}