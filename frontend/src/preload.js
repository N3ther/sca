const sca = require('../native/sca')
const fs = require('fs')
const {dialog, ipcRenderer} = require('electron')
const os = require('os')

/// Native module.
window.sca = sca

/// Make sure the page knows it's not in a browser.
window.electron = true

/// This is not the most secure way of doing it, but someone's
/// system gets compromised because they put random js in the
/// console of a sound change applier, then they've brought that
/// upon themselves.
window.__fs = fs
window.__os = os

window.__homedir = os.homedir()

/// Write a message to the terminal.
///
/// This way, we don't have to open devtools every time we
/// want to log something; this also prevents the console
/// from interfering with measurements such as `innerHeight`.
window.__dbg = (...msg) => {
    if (Array.isArray(msg)) msg.forEach(m => ipcRenderer.send('log-to-console', m))
    else ipcRenderer.send('log-to-console', msg)
}

/// Determine whether we're running in debug mode
ipcRenderer.on('__debug_mode_p', (_, debug_mode) => { window.__debug_mode = debug_mode })
ipcRenderer.send('__debug_mode_p')

window.__eval = (code) => {
    ipcRenderer.send('eval', code)
}

/// IPC Calls.
window.__ipc_promise_table = new Map
window.__ipc_id = 0

window.__ipc_call = (fun, params = {}) => {
    return new Promise((resolve) => {
        window.__ipc_promise_table.set(window.__ipc_id, resolve)
        ipcRenderer.send('__ipc_call', {fun: fun, params: params, id: window.__ipc_id++})
    })
}

ipcRenderer.on('__ipc_return', (_, data) => {
    let {ret, id} = data
    let p = window.__ipc_promise_table.get(id)
    window.__ipc_promise_table.delete(id)
    p(ret)
})

/// Version
window.__versions = [process.versions.chrome, process.versions.node, process.versions.electron]

/// Filesystem
const settings_file_path = __dirname + '/../settings/settings.json'
window.read_settings_file = () => {
    if (!fs.existsSync(settings_file_path)) return '{}'
    return fs.readFileSync(settings_file_path).toString('utf8')
}

window.write_to_settings_file = (settings) => {
    fs.writeFileSync(settings_file_path, settings)
}


